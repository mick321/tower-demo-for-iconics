#include "_precompiled.h"
#include "TowerDemoForIconics.h"

// ------------------------------------------------------------------
// Windows application entry point.
int WINAPI WinMain(_In_ HINSTANCE hInstance,
  _In_ HINSTANCE hPrevInstance,
  _In_ LPSTR lpCmdLine,
  _In_ int nCmdShow)
{
  // when on debug, this function prepares resources used for checking memory leaks of engine objects
  InitCheckGenericAllocations();

  // Instantiate application class and run it
  int rtnExitCode;
  {
    TowerDemoForIconics towerDemoForIconics;
    rtnExitCode = towerDemoForIconics.Run();
  }

  // when on debug, check for memory leaks of engine objects
  CheckGenericZeroAllocations();
  // when on debug, this function releases resources used for checking memory leaks of engine objects
  DeInitCheckGenericAllocations();
  return rtnExitCode;
}