#include "_precompiled.h"
#include "CameraController.h"
#pragma hdrstop

// Macro - get array length
#define ARRAY_LEN(arr) (sizeof(arr)/sizeof(*(arr)))

#define CAMERA_TERRAIN_LIMITATION_OFFSET 2.0f
#define CAMERA_SIDE_LIMITAION_CENTER 64.f
#define CAMERA_SIDE_LIMITAION_DISTANCE 25.f
#define CAMERA_CEILING_LIMITATION_VALUE 17.f

// constructor, initial position can be defined here
CameraController::CameraController(CGeneric* owner, const glm::vec3& initialPosition) : CModule(owner)
{
  labelDebug = new CLabel(core->SceneMan->GUI);
  labelDebug->Left(0.f);
  labelDebug->Top(0.f);
  labelDebug->Width(1.f);
  labelDebug->Height(0.2f);
#ifndef _DEBUG
  labelDebug->Visible(false);
#endif

  viewerPosition = initialPosition;
  viewerTilt = 0.f;
  viewerRotation = 0.f;

  CInputValueFloat* rotationMouseX[] = {
    core->InputMan->CreateInput_MouseLockedPositionOffsetFloat(0),
    core->InputMan->CreateInput_GamepadAxisFloat(SDL_CONTROLLER_AXIS_RIGHTX, -10.f, 10.f)
  };
  inputCameraMouseX = core->InputMan->CreateInput_ArrayOp<CInputArrayOpPlus<float, ARRAY_LEN(rotationMouseX)> >(rotationMouseX);

  CInputValueFloat* rotationMouseY[] = {
    core->InputMan->CreateInput_MouseLockedPositionOffsetFloat(1),
    core->InputMan->CreateInput_GamepadAxisFloat(SDL_CONTROLLER_AXIS_RIGHTY, -10.f, 10.f)
  };
  inputCameraMouseY = core->InputMan->CreateInput_ArrayOp<CInputArrayOpPlus<float, ARRAY_LEN(rotationMouseY)> >(rotationMouseY);

  CInputValueFloat* forwardBackward[] = {
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_W, 0.f, 1.f),
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_UP, 0.f, 1.f),
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_S, 0.f, -1.f),
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_DOWN, 0.f, -1.f),
    core->InputMan->CreateInput_GamepadAxisFloat(SDL_CONTROLLER_AXIS_LEFTY, 1.f, -1.f),
    core->InputMan->CreateInput_GamepadButtonFloat(SDL_CONTROLLER_BUTTON_DPAD_UP, 0.f, 1.f),
    core->InputMan->CreateInput_GamepadButtonFloat(SDL_CONTROLLER_BUTTON_DPAD_DOWN, 0.f, -1.f)
  };
  inputForwardBackward = core->InputMan->CreateInput_ArrayOp<CInputArrayOpPlus<float, ARRAY_LEN(forwardBackward)> >(forwardBackward);

  CInputValueFloat* leftRight[] = {
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_A, 0.f, -1.f),
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_D, 0.f, 1.f),
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_LEFT, 0.f, -1.f),
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_RIGHT, 0.f, 1.f),
    core->InputMan->CreateInput_GamepadAxisFloat(SDL_CONTROLLER_AXIS_LEFTX, -1.f, 1.f),
    core->InputMan->CreateInput_GamepadButtonFloat(SDL_CONTROLLER_BUTTON_DPAD_RIGHT, 0.f, 1.f),
    core->InputMan->CreateInput_GamepadButtonFloat(SDL_CONTROLLER_BUTTON_DPAD_LEFT, 0.f, -1.f)
  };
  inputLeftRight = core->InputMan->CreateInput_ArrayOp<CInputArrayOpPlus<float, ARRAY_LEN(leftRight)> >(leftRight);

  CInputValueFloat* upDown[] = {
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_SPACE, 0.f, -1.f),
    core->InputMan->CreateInput_KeyboardFloat(SDL_SCANCODE_LCTRL, 0.f, 1.f),
    core->InputMan->CreateInput_GamepadAxisFloat(SDL_CONTROLLER_AXIS_TRIGGERLEFT, 0.f, 1.f),
    core->InputMan->CreateInput_GamepadAxisFloat(SDL_CONTROLLER_AXIS_TRIGGERRIGHT, 0.f, -1.f),
  };
  inputUpDown = core->InputMan->CreateInput_ArrayOp<CInputArrayOpPlus<float, ARRAY_LEN(upDown)> >(upDown);

  LibraryStart();
}

void CameraController::AddCameraPresentationPoint(const float t, const glm::vec3& pos, const float tilt, const float rot)
{
  splinePos.AddPoint(t, pos);
  splineTilt.AddPoint(t, tilt);
  splineRotation.AddPoint(t, rot);

  if (t > cameraMaxT)
    cameraMaxT = t;
}

// limits camera position
void CameraController::LimitCamera()
{
  if (!terrain)
    return;

  // terrain limitation
  float terrainElevation = terrain->GetElevation(viewerPosition.x, viewerPosition.z) + CAMERA_TERRAIN_LIMITATION_OFFSET;
  if (viewerPosition.y < terrainElevation)
    viewerPosition.y = terrainElevation;

  // side limitation
  glm::vec2 positionFromCenter = glm::vec2(viewerPosition.x, viewerPosition.z) - CAMERA_SIDE_LIMITAION_CENTER;
  float distanceFromCenter = glm::length(positionFromCenter);
  if (distanceFromCenter > CAMERA_SIDE_LIMITAION_DISTANCE)
  {
    glm::vec2 sidePos = glm::normalize(positionFromCenter) * CAMERA_SIDE_LIMITAION_DISTANCE;
    viewerPosition.x = sidePos.x + CAMERA_SIDE_LIMITAION_CENTER;
    viewerPosition.z = sidePos.y + CAMERA_SIDE_LIMITAION_CENTER;
  }

  // ceiling limitation
  if (viewerPosition.y > CAMERA_CEILING_LIMITATION_VALUE)
    viewerPosition.y = CAMERA_CEILING_LIMITATION_VALUE;

  // central tree group
  const glm::vec2 treeGroupXZ(56.5f + 10.f, 54.5f + 1.f);
  glm::vec3 closestValidPos;
  if (mathutils::PointInCapsule(glm::vec3(treeGroupXZ.x, 0.f, treeGroupXZ.y), glm::vec3(treeGroupXZ.x, 7.5f, treeGroupXZ.y),
    6.f, viewerPosition, closestValidPos))
  {
    viewerPosition = closestValidPos;
  }
}


void CameraController::CameraFreeLook(TSceneIdentifier sceneId)
{
  viewerRotation -= inputCameraMouseX->Value() * 0.003f;
  viewerTilt -= inputCameraMouseY->Value() * 0.003f;
  float halfPi = glm::pi<float>() * 0.5f;
  if (viewerTilt >= halfPi)
    viewerTilt = halfPi;
  if (viewerTilt <= -halfPi)
    viewerTilt = -halfPi;

  glm::vec3 positionChange(0);
  positionChange.x -= -InputTolerance(inputForwardBackward->Value()) * -glm::sin(viewerRotation) * glm::cos(viewerTilt);
  positionChange.z -= InputTolerance(inputForwardBackward->Value()) * glm::cos(viewerRotation) * glm::cos(viewerTilt);
  positionChange.y -= InputTolerance(inputForwardBackward->Value()) * -glm::sin(viewerTilt);
  positionChange.y -= InputTolerance(inputUpDown->Value());
  positionChange.x -= InputTolerance(inputLeftRight->Value()) * -glm::cos(viewerRotation);
  positionChange.z -= InputTolerance(inputLeftRight->Value()) * glm::sin(viewerRotation);

  if (glm::length2(positionChange) > 0.f)
  {
    positionChange = glm::normalize(positionChange);
    float viewerSpeed = sceneId == sceneElevator ? 0.0025f : 0.0125f;
    viewerPosition += positionChange * viewerSpeed * (float)core->TimeMan->GetMainTime()->GetDiff();
  }

  if (cameraLimitation)
    LimitCamera();

  core->SceneMan->ActiveScene->GetCurrentCamera()->LocalMatrix =
    glm::rotate(-viewerTilt, glm::vec3(1.f, 0.f, 0.f)) *
    glm::rotate(-viewerRotation, glm::vec3(0.f, 1.f, 0.f)) *
    glm::translate(-viewerPosition);
  core->SceneMan->ActiveScene->GetCurrentCamera()->ApplyTransformations();

  while (viewerRotation < 0.f)
    viewerRotation += 2 * glm::pi<float>();
  while (viewerRotation > 2 * glm::pi<float>())
    viewerRotation -= 2 * glm::pi<float>();
}

void CameraController::CameraPresentation()
{
  t += (float)core->TimeMan->GetMainTime()->GetDiff() * 0.0003f * glm::max(splineCameraSpeed.GetValue(t), 0.005f) / 9.f;// *slowFactor;
  if (t > cameraMaxT + 1.f / 9.f)
    t = 1.0f / 9.f;

  viewerPosition = splinePos.GetValue(t);
  viewerTilt = splineTilt.GetValue(t);
  viewerRotation = splineRotation.GetValue(t);
}

// sets proper camera position, rotation and tilt according to camera mode (free look, presentation)
void CameraController::SetupCamera(TCameraMode cameraMode, TSceneIdentifier sceneId)
{
  labelDebug->SetText(
    "X:" + strutils::FloatToStr(viewerPosition.x) +
    "  Y:" + strutils::FloatToStr(viewerPosition.y) +
    "  Z:" + strutils::FloatToStr(viewerPosition.z) +
    "\ntilt:" + strutils::FloatToStr(viewerTilt) +
    "  rotation:" + strutils::FloatToStr(viewerRotation));

  switch (cameraMode)
  {
  case cameraFree: CameraFreeLook(sceneId); break;
  case cameraAuto: CameraPresentation(); break;
  default: break;
  };

  core->SceneMan->ActiveScene->GetCurrentCamera()->LocalMatrix =
    glm::rotate(-viewerTilt, glm::vec3(1.f, 0.f, 0.f)) *
    glm::rotate(-viewerRotation, glm::vec3(0.f, 1.f, 0.f)) *
    glm::translate(-viewerPosition);
  core->SceneMan->ActiveScene->GetCurrentCamera()->ApplyTransformations();
}

// turns on/off camera limitation
void CameraController::CameraLimitation(bool allowed)
{ 
  if (GlobalLibrary->Contains("#meadow_terraindata"))
    terrain = GlobalLibrary->Fetch<CTerrain>(this, "#meadow_terraindata");

  cameraLimitation = allowed; 
}

void CameraController::Destroy()
{
  LibraryStop();
}

void CameraController::PreparePresentation(TSceneIdentifier sceneId)
{
  // reset presentation parameter
  //t = 0.f;

  splinePos = mathutils::Spline3Df();
  splineTilt = mathutils::Spline1Df();
  splineRotation = mathutils::Spline1Df();
  splineCameraSpeed = mathutils::Spline1Df();

  splinePos.spline1d[0].extrapolationIsConstant = true;
  splinePos.spline1d[1].extrapolationIsConstant = true;
  splinePos.spline1d[2].extrapolationIsConstant = true;
  splineTilt.extrapolationIsConstant = true;
  splineRotation.extrapolationIsConstant = true;
  splineCameraSpeed.extrapolationIsConstant = true;

  if (sceneId == sceneMeadow)
  {
    AddCameraPresentationPoint(0.f / 9.f, glm::vec3(66.71f, 7.683f, 39.147f), 0.558f, 0.24f);
    AddCameraPresentationPoint(1.f / 9.f, glm::vec3(68.14f, 9.64f, 39.795f), -0.531f, 2.439f);
    AddCameraPresentationPoint(2.f / 9.f, glm::vec3(78.6f, 2.442f, 49.73f), -0.16f, 3.378f);
    AddCameraPresentationPoint(3.f / 9.f, glm::vec3(86.71f, 0.365f, 73.466f), -0.048f, 1.824f);
    AddCameraPresentationPoint(4.f / 9.f, glm::vec3(44.736f, 5.608f, 79.934f), -0.426f, 5.4512f - 2 * glm::pi<float>());
    AddCameraPresentationPoint(5.f / 9.f, glm::vec3(49.609f, 2.54f, 61.706f), -0.057f, 0.066f);
    AddCameraPresentationPoint(6.f / 9.f, glm::vec3(69.487f, 11.316f, 51.96f), -0.684f, 2.64f - 2 * glm::pi<float>());
    AddCameraPresentationPoint(7.f / 9.f, glm::vec3(77.1964f, -0.84754f, 76.109f), 0.441f, 3.399f - 2 * glm::pi<float>());
    AddCameraPresentationPoint(8.f / 9.f, glm::vec3(62.152f, 4.027f, 78.645f), -0.5f, 1.72618f - 2 * glm::pi<float>());
    AddCameraPresentationPoint(9.f / 9.f, glm::vec3(47.182f, 11.572f, 86.538f), -0.021f, 5.371986f - 4 * glm::pi<float>());

    splinePos.PrepareC2();
    splineTilt.PrepareC2();
    splineRotation.PrepareC2();

    // make the camera movement smooth at the final step (set derivation to zero and recompute polynomial coeffs).
    splinePos.spline1d[0].stored_values[splinePos.spline1d[0].stored_values.size() - 2].dfx_dx_right = 0.f;
    splinePos.spline1d[1].stored_values[splinePos.spline1d[1].stored_values.size() - 2].dfx_dx_right = 0.f;
    splinePos.spline1d[2].stored_values[splinePos.spline1d[2].stored_values.size() - 2].dfx_dx_right = 0.f;
    splineTilt.stored_values[splineTilt.stored_values.size() - 2].dfx_dx_right = 0.f;
    splineRotation.stored_values[splineRotation.stored_values.size() - 2].dfx_dx_right = 0.f;

    //splinePos.spline1d[0].stored_values[splinePos.spline1d[0].stored_values.size() - 2].compute_coeffs(
    //  splinePos.spline1d[0].stored_values[splinePos.spline1d[0].stored_values.size() - 1].x,
    //  splinePos.spline1d[0].stored_values[splinePos.spline1d[0].stored_values.size() - 1].fx);
    //splinePos.spline1d[1].stored_values[splinePos.spline1d[1].stored_values.size() - 2].compute_coeffs(
    //  splinePos.spline1d[1].stored_values[splinePos.spline1d[1].stored_values.size() - 1].x,
    //  splinePos.spline1d[1].stored_values[splinePos.spline1d[1].stored_values.size() - 1].fx);
    //splinePos.spline1d[2].stored_values[splinePos.spline1d[2].stored_values.size() - 2].compute_coeffs(
    //  splinePos.spline1d[2].stored_values[splinePos.spline1d[2].stored_values.size() - 1].x,
    //  splinePos.spline1d[2].stored_values[splinePos.spline1d[2].stored_values.size() - 1].fx);
    //splineTilt.stored_values[splineTilt.stored_values.size() - 2].compute_coeffs(
    //  splineTilt.stored_values[splineTilt.stored_values.size() - 1].x,
    //  splineTilt.stored_values[splineTilt.stored_values.size() - 1].fx);
    //splineRotation.stored_values[splineRotation.stored_values.size() - 2].compute_coeffs(
    //  splineRotation.stored_values[splineRotation.stored_values.size() - 1].x,
    //  splineRotation.stored_values[splineRotation.stored_values.size() - 1].fx);

    splineCameraSpeed.AddPoint(0.f / 9.f, 0.f);
    splineCameraSpeed.AddPoint(0.1f / 9.f, 0.4f);
    splineCameraSpeed.AddPoint(2.f / 9.f, 0.7f);
    splineCameraSpeed.AddPoint(4.f / 9.f, 0.4f);
    splineCameraSpeed.AddPoint(5.f / 9.f, 0.2f);
    splineCameraSpeed.AddPoint(6.f / 9.f, 0.5f);
    splineCameraSpeed.AddPoint(8.f / 9.f, 0.4f);
    splineCameraSpeed.AddPoint(8.3f / 9.f, 0.7f);
    splineCameraSpeed.AddPoint(8.6f / 9.f, 0.3f);
    splineCameraSpeed.PrepareSmoothStep();
  } 
  else if (sceneId == sceneElevator)
  {
    AddCameraPresentationPoint(0.0f, glm::vec3(64.44f, 3.22f, 59.42f), -0.127f, 3.06f);
    AddCameraPresentationPoint(0.1f, glm::vec3(64.64f, 2.765f, 63.62f), 0.181f, 1.88f);
    AddCameraPresentationPoint(0.2f, glm::vec3(59.25f, 5.18f, 63.62f), -0.45f, 6.14f - 2 * glm::pi<float>());
    AddCameraPresentationPoint(0.3f, glm::vec3(58.1f, 5.07f, 60.09f), 0.2f, 4.39f - 2 * glm::pi<float>());
    AddCameraPresentationPoint(0.4f, glm::vec3(61.13f, 7.01f, 59.1f), -0.1f, 4.9f - 2 * glm::pi<float>());
    AddCameraPresentationPoint(0.5f, glm::vec3(64.42f, 6.93f, 60.85f), -0.11f, 0.175f);
    AddCameraPresentationPoint(0.6f, glm::vec3(65.18f, 6.77f, 64.23f), 0.17f, 0.9f);
    AddCameraPresentationPoint(0.8f, glm::vec3(64.18f, 6.67f, 63.23f), 0.1f, 1.2f);
    AddCameraPresentationPoint(1.0f, glm::vec3(64.98f, 6.97f, 62.23f), 0.1f, 1.2f);

    splinePos.PrepareC2();
    splineTilt.PrepareC2();
    splineRotation.PrepareC2();

    splineCameraSpeed.AddPoint(0.f, 0.0f);
    splineCameraSpeed.AddPoint(0.01f, 0.3f);
    splineCameraSpeed.AddPoint(0.04f, 0.6f);
    splineCameraSpeed.AddPoint(0.1f, 0.2f);
    splineCameraSpeed.AddPoint(0.2f, 1.2f);
    splineCameraSpeed.AddPoint(0.3f, 1.8f);
    splineCameraSpeed.AddPoint(0.4f, 1.9f);
    splineCameraSpeed.AddPoint(0.5f, 1.4f);
    splineCameraSpeed.AddPoint(0.85f, 1.9f);
    splineCameraSpeed.AddPoint(0.95f, 0.6f);
    splineCameraSpeed.AddPoint(1.0f, 0.3f);
    splineCameraSpeed.PrepareSmoothStep();
  }
}

void CameraController::OnPossibleSimpleDraw()
{
  CSimpleDraw* sd = core->SceneMan->SimpleDraw;
  (void)sd;

  // drawing debugging primitives

  /*sd->Begin(gapi::pmLineStrip);
  for (float p = 0; p < cameraMaxT; p += 0.005f)
    sd->AddVertex(splinePos.GetValue(p));
  sd->End();*/

  //sd->Begin(gapi::pmTriangles);
  //sd->SetColor(glm::vec3(1.f, 1.f, 1.f));    // tilt, rotation
  //sd->AddCube(66.71f, 7.683f, 39.147f, 1.f); // 0.558f, 0.24f
  //sd->SetColor(glm::vec3(1.f, 0.f, 0.f));
  //sd->AddCube(68.14f, 9.64f, 39.795f, 1.f);  // -0.531f, 2.439
  //sd->SetColor(glm::vec3(0.5f, 0.f, 0.f));
  //sd->AddCube(78.6f, 2.442f, 49.73f, 1.f);   // -0.16f, 3.378f
  //sd->SetColor(glm::vec3(0.2f, 0.f, 0.f));
  //sd->AddCube(86.71f, 0.365f, 73.466f, 1.f); // -0.048f, 1.824f 
  //sd->SetColor(glm::vec3(0.f, 1.f, 0.f));
  //sd->AddCube(44.736f, 5.608f, 79.934f, 1.f);// -0.426f, 5.2512f
  //sd->SetColor(glm::vec3(0.f, 0.5f, 0.f));
  //sd->AddCube(49.609f, 2.84f/*dolu*/, 61.706f, 1.f);// -0.057f, 0.066
  //sd->SetColor(glm::vec3(0.f, 0.2f, 0.f));
  //sd->AddCube(69.487f, 11.316f, 51.96f, 1.f);// -0.684f, 2.64f
  //sd->SetColor(glm::vec3(0.f, 0.0f, 1.f));
  //sd->AddCube(77.1964f, -2.34754f, 76.109f, 1.f);// 0.441f, 3.399f
  //sd->SetColor(glm::vec3(0.f, 0.f, 0.5f));
  //sd->AddCube(57.152f, 0.097f/*dolu*/, 80.645f, 1.f);// 0.063f, 1.72618f
  //sd->SetColor(glm::vec3(0.f, 0.f, 0.0f));
  //sd->AddCube(53.182f, 11.572f, 86.538f, 1.f);// 0.021f, 5.171986f
  //sd->End();
}

void CameraController::ResetInitialPosition(TSceneIdentifier sceneId) 
{ 
  t = 0.f;

  if (sceneId == sceneMeadow)
  {
    core->SceneMan->ActiveScene->GetCurrentCamera()->SetPerspectiveMode(0.f, 75.f, 1.f, 200.f);
    viewerPosition = glm::vec3(64.f, 2.f, 64.f);
    viewerTilt = 0.f;
    viewerRotation = 0.f;
  }
  else if (sceneId == sceneElevator)
  {
    core->SceneMan->ActiveScene->GetCurrentCamera()->SetPerspectiveMode(0.f, 75.f, 0.1f, 50.f);
    viewerPosition = glm::vec3(64.f, 2.f, 64.f);
    viewerTilt = 0.f;
    viewerRotation = 0.f;
  }
}