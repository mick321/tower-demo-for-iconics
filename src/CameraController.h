#pragma once
#include "Common.h"

// Takes care for camera position, limitation etc.
class CameraController : public CModule
{
private:
  // maximum value of parameter t used for interpolation of camera
  float cameraMaxT = 0.f;
  // parameter t used for interpolation of camera
  float t = 0.0f;
  // spline based on discrete 3D camera positions in time
  mathutils::Spline3Df splinePos;
  // spline based on discrete camera tilt in time
  mathutils::Spline1Df splineTilt;
  // spline based on discrete camera Y rotation in time
  mathutils::Spline1Df splineRotation;
  // spline based on discrete camera movement speed in time
  mathutils::Spline1Df splineCameraSpeed;
  // position of camera
  glm::vec3 viewerPosition;
  // tilt of camera
  float viewerTilt;
  // rotation of camera
  float viewerRotation;
  // switch between camera limitation  
  bool cameraLimitation = false;
  // pointer to terrain (used for camera limitation)
  CHandle<CTerrain> terrain = NULL;
  // gui label for debugging
  CLabel* labelDebug;

  // limits camera position
  void LimitCamera();
  // switch to free look mode
  void CameraFreeLook(TSceneIdentifier sceneId);
  // switch to automatic mode (camera is moving through scene)
  void CameraPresentation();
  // add camera control point
  void AddCameraPresentationPoint(const float t, const glm::vec3& pos, const float tilt, const float rot);

  inline static const float InputTolerance(const float x) { if (x > -0.25f && x < 0.25f) return 0.f; else return x; }
public:
  // inputs for keyboard, mouse and gamepad
  CInputValueFloat* inputCameraMouseX;
  CInputValueFloat* inputCameraMouseY;
  CInputValueFloat* inputForwardBackward;
  CInputValueFloat* inputLeftRight;
  CInputValueFloat* inputUpDown;

  // constructor, initial position can be defined here
  CameraController(CGeneric* owner, const glm::vec3& initialPosition = glm::vec3(0));
  // destructor
  inline virtual ~CameraController() { Destroy(); }

  // sets proper camera position, rotation and tilt according to camera mode (free look, presentation)
  void SetupCamera(TCameraMode cameraMode, TSceneIdentifier sceneId);

  // sets initial camera position
  void ResetInitialPosition(TSceneIdentifier sceneId);

  // turns on/off camera limitation
  void CameraLimitation(bool allowed);

  // called when engine shuts down
  void Destroy();

  // called when engine can perform simple drawing (that can be used for debugging)
  virtual void OnPossibleSimpleDraw() override;

  // prepare scene presentation (set up camera paths)
  void PreparePresentation(TSceneIdentifier sceneId);
};