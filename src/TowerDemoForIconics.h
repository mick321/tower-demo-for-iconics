#pragma once
#include "_precompiled.h"
#include "Common.h"

class CameraController;
class GuiScreen;

// Class defining application behavior, descendant of engine core.
//
// Engine provides "event hooks" - virtual methods. 
// By overriding them, we define own behavior of our app.
class TowerDemoForIconics : public CTowerCORE
{
private:
  CGeneric resourceHolder; // instance holding managed resources
  CameraController* cameraController;
  GuiScreen* gui; // gui
  CImageButton* imgBlack; // image for fading process
  FadeoutData* fadeout; // data for fading process

  TSceneIdentifier currentScene;
  TCameraMode cameraMode;
  
  // Load all resources needed for demo.
  void LoadAllResources();
  // Prepare meadow scene.
  void PrepareMeadow();
  // Prepare elevator scene.
  void PrepareElevator();
  // Prepare trees on meadow scene.
  void PrepareMeadowTrees(const glm::vec2& centerPoint, const float minR, const float maxR, const CTerrain* terrainData);
  // Creates flowers on terrain at given coordinates
  void CreateFlowers(float x, float z);
protected:
  // Processing universal engine messages.
  virtual bool ProcessMessageEx(uint32_t msg, void* blob, uint32_t blobsize, uint32_t flags) override;
public:
  // Constructor should not define any behavior, all preparations are done in member function OnCreate.
  inline TowerDemoForIconics() 
    : CTowerCORE("BluePulsar", "TowerDemoForIconics"), resourceHolder(this), currentScene(sceneUnknown) {}
  // Destructor should not define any behavior, all deletions are done in member function OnDestroy.
  inline virtual ~TowerDemoForIconics() { }

  // Engine event hook, called right after engine initialization.
  virtual void OnCreate();
  // Engine event hook, called before engine deinitialization.
  virtual void OnDestroy();
  // Engine event hook, called on every frame start.
  virtual void OnEveryFrameStart();
  // Engine event hook, called when debugging drawing is possible.
  virtual void OnPossibleSimpleDraw();

  // Switch to next available scene.
  void NextScene();
  // Camera in free mode.
  void CameraFreeMode();
  // Camera in automatic mode.
  void CameraAutoMode();
  // Camera stopped and cursor is not bound to center of screen anymore.
  void CameraStop();
  // Set direction of main light.
  void SetMainLightDirection(const glm::vec3& lightDirection);
  // Set color of main light.
  void SetMainLightColor(const glm::vec3& lightColor);
  // Camera mode getter.
  inline const TCameraMode GetCameraMode() const
  {
    return cameraMode;
  }
};

// --------------

// Small functor class for generating terrain
class CEvalTerrainGen : public terrainutils::CEvalIn2Out1PerlinNoise
{
private:
  float w, h;
public:
  // constructor
  CEvalTerrainGen(int levels, int sizex, int sizey, const glm::vec2& seed)
    : CEvalIn2Out1PerlinNoise(levels, sizex, sizey, seed)
  {
    w = (float)sizex;
    h = (float)sizey;
  }

  // functor method
  virtual float operator()(float x, float y)
  {
    float rtnParent = CEvalIn2Out1PerlinNoise::operator()(x, y);
    float rtn = 0.f;
    if (x < 1.f || y < 1.f || x > w - 1.f || y > h - 1.f)
      rtn += 1.5f;
    x -= w * 0.5f;
    y -= h * 0.5f;
    rtn -= 2.f * Amplitude * glm::pow(glm::e<float>(), -0.5f*(x*x + y*y) / (w + h));
    float lerpFactor = glm::clamp(glm::sqrt(x*x + y*y) / glm::min(w, h), 0.f, 1.f);
    rtn = glm::lerp(rtnParent, rtn, lerpFactor);
    return rtn;
  }
};


// --------------

// Class for spawning objects on "ring". Used for spawning trees.
class PointsFi
{
private:
  float minR, maxR;   // minimal and maximal radius
  float itormulcoef;  // conversion factor
  float rtoimulcoef;  // conversion factor
  float* all_fi;      // array of phases
  float* all_r;       // array of radiuses
  int valuecount;     // number of items in each array
public:

  // Constructor, pass min-max radius of ring.
  PointsFi(const float minR, const float maxR) : minR(minR), maxR(maxR)
  {
    valuecount = (int)(maxR - minR);
    all_fi = new float[valuecount];
    all_r = new float[valuecount];
    itormulcoef = (maxR - minR) / ((float)valuecount);
    rtoimulcoef = 1.f / itormulcoef;
    for (int i = 0; i < valuecount; i++)
    {
      all_r[i] = minR + i * itormulcoef;
      all_fi[i] = 0.f;
    }
  }

  // Destructor.
  ~PointsFi()
  {
    delete all_fi;
    delete all_r;
  }

  // Compute squared distance between two point in radial basis.
  inline static const float radialDist2(const float r1, const float fi1, const float r2, const float fi2)
  {
    return r1 * r1 + r2 * r2 - 2.f * r1 * r2 * glm::cos(fi1 - fi2);
  }

  // Compute distance between two point in radial basis.
  inline static const float radialDist(const float r1, const float fi1, const float r2, const float fi2)
  {
    return glm::sqrt(radialDist2(r1, fi1, r2, fi2));
  }

  // Find index of radius with minimal current phase.
  int findMinFiIndex()
  {
    float minval = 1e10f;
    int mini = 0;
    for (int i = 0; i < valuecount; i++)
      if (all_fi[i] < minval)
      {
      mini = i;
      minval = all_fi[i];
      }

    return mini;
  }

  // Find radius with minimal current phase.
  float findMinFiR()
  {
    return minR + (float)findMinFiIndex() * itormulcoef;
  }

  // Test colision between ring and circle of radius "size" placed on point ("r", "fi").
  bool testColision(const float r, const float fi, const float size)
  {
    for (int i = 0; i < valuecount; i++)
    {
      if (radialDist2(r, fi, all_r[i], all_fi[i]) < size * size)
        return true;
    }

    return false;
  }

  // Enumeration - result of placing an object.
  enum TPlacingResult
  {
    presFailEnd, // failed, no more room
    presFail, // failed for this time, try again
    presOk
  };

  // Query: placing object inside this ring. Answer: ok/try somewhere else/no chance.
  TPlacingResult placeObject(const float size, float& out_r, float& out_fi)
  {
    int index = findMinFiIndex();
    if (all_fi[index] > 2.f * glm::pi<float>())
      return presFailEnd;

    float r = all_r[index];
    float fi = all_fi[index];

    int isize = (int)(2.f * size * rtoimulcoef);
    int fromi = index - isize;
    if (fromi < 0)
      fromi = 0;
    int toi = index + isize;
    if (toi >= valuecount)
      toi = valuecount - 1;

    for (int i = fromi; i <= toi; i++)
    {
      fi = glm::min(fi, all_fi[i]);
    }

    while (fi < 2.f * glm::pi<float>() && testColision(r, fi, size))
    {
      fi += (size) / r;
    }

    float parabola_coeff = 1.f / (0.25f * (toi - fromi) * (toi - fromi));
    for (int i = fromi; i <= toi; i++)
    {
      float coeff = glm::max(0.f, 1.f - (float)((i - index) * (i - index)) / parabola_coeff);
      all_fi[index] = fi + coeff * size / r;
    }

    out_r = r;
    out_fi = fi;

    return (fi - size / r < 2.f * glm::pi<float>()) ? presOk : presFail;
  }

};