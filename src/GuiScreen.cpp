#include "_precompiled.h"
#include "GuiScreen.h"
#include "TowerDemoForIconics.h"
#include "Common.h"

//constructor 
GuiScreen::GuiScreen(CGeneric* Owner) : CModule(Owner)
{
  LibraryStart();
  // show fps
  CFpsLabel* fpsLabel = new CFpsLabel(core->SceneMan->GUI, false);
  fpsLabel->Top(0.45f);
  fpsLabel->Width(0.8f);
  fpsLabel->SetBackground(glm::vec4(0.f));
  fpsLabel->AcceptInput = CGUIElement::TInputAcceptation::Ignore;

  // show engine logo
  CImageButton* imageButton = new CImageButton(core->SceneMan->GUI, rectf(0.f, 0.f, 1.f, 1.f));
  imageButton->Right(0.1f); imageButton->Top(0.15f);
  imageButton->Width(0.6f); imageButton->Height(0.3f);
  imageButton->CustomTexture = GlobalLibrary->Fetch<CTexture>(this, "data/tex/tower_logo_white.png");
  imageButton->Visible(imageButton->CustomTexture.IsValid()); // hide logo if texture not found
  imageButton->AcceptInput = CGUIElement::TInputAcceptation::Ignore;

  layout = new CColumnLayout(core->SceneMan->GUI);

  btnLDirection = new CButtonEx(layout);
  btnLDirection->Height(0.1f);
  btnLDirection->SetText("Light Position");
  btnLDirection->OnMouseClick.bind(this, &GuiScreen::OnClick_Button);
  btbLColour = new CButtonEx(layout);
  btbLColour->Height(0.1f);
  btbLColour->SetText("Light Color");
  btbLColour->OnMouseClick.bind(this, &GuiScreen::OnClick_Button);
  btnScene = new CButtonEx(layout);  
  btnScene->Height(0.1f);
  btnScene->SetText("Next Scene");
  btnScene->OnMouseClick.bind(this, &GuiScreen::OnClick_Button);
  btnMotion = new CButtonEx(layout);
  btnMotion->Height(0.1f);
  btnMotion->Width(btnScene->Width() * 1.5f);
  btnMotion->SetText("Free Look/Automatic Camera");
  btnMotion->OnMouseClick.bind(this, &GuiScreen::OnClick_Button);
  btnExit = new CButtonEx(layout);
  btnExit->Height(0.1f);
  btnExit->SetText("Exit");
  btnExit->OnMouseClick.bind(this, &GuiScreen::OnClick_Button);
  
  layout->ArrangeElements();
  layout->Right(3*btnExit->Width() + btnMotion->Width() + 4 * layout->Spacing() + 0.1f);
  layout->Top(0);

  lblAuthors = new CLabel(core->SceneMan->GUI);
  lblAuthors->Right(0.f);
  lblAuthors->Bottom(0.f);
  lblAuthors->Width(1.6f);
  lblAuthors->Height(0.1f);
  lblAuthors->SetBackground(glm::vec4(0.f));
  lblAuthors->SetText("Vojtech Kosar, Katerina Stollova, Michal Zak and Ondrej Martinek, November 2014");
  font_options fo = CLabel::GetDefaultFontOptions();
  fo.align = text_align_right;
  lblAuthors->SetFontOptions(fo);

  dlgLColour = new CColorDialog(core->SceneMan->GUI);
  dlgLColour->Visible(false);
  dlgLColour->Left(0.5f);
  dlgLColour->OnColorChange.bind(this, &GuiScreen::OnColorChange);
  dlgLColour->DestroyOnClose = false;
  dlgLColour->SetRGB(defaultLightColor.x, defaultLightColor.y, defaultLightColor.z);
  dlgLDirection = new CScrollValuesDialog(core->SceneMan->GUI, 2);
  dlgLDirection->Visible(false);
  dlgLDirection->Top(dlgLColour->Top());
  dlgLDirection->Left(dlgLColour->Left() + dlgLColour->Width() + 0.05f);
  dlgLDirection->OnValueChange.bind(this, &GuiScreen::OnDirectionChange);
  dlgLDirection->DestroyOnClose = false;
  dlgLDirection->SetValue(0, -defaultLightDir.x);
  dlgLDirection->SetValue(1, -defaultLightDir.z);
  dlgLDirection->SetCaption("Light Position");
}


int GuiScreen::OnClick_Button(CGUIElement* elem, uint32_t flags)
{
  if (elem == btnExit)
    ProcessMessage(MSG_DEMO_EXIT);
  else if (elem == btnMotion)
    ProcessMessage(MSG_DEMO_CAMERA);
  else if (elem == btnScene)
    ProcessMessage(MSG_DEMO_NEXTSCENE);
  else if (elem == btbLColour)
    dlgLColour->Visible(!dlgLColour->Visible());
  else if (elem == btnLDirection)
    dlgLDirection->Visible(!dlgLDirection->Visible());
  return 0;
}

void GuiScreen::OnColorChange(CColorDialog* elem)
{
  BlobDemoLightColor color;
  elem->GetRGB(color.color.x, color.color.y, color.color.z);
  ProcessMessage(MSG_DEMO_LIGHTCOLOR, &color, sizeof(BlobDemoLightColor));
}

void GuiScreen::OnDirectionChange(CScrollValuesDialog* elem, int index, float value)
{
  BlobDemoLightPos position;
  position.position.x = elem->GetValue(0);
  position.position.z = elem->GetValue(1);
  ProcessMessage(MSG_DEMO_LIGHTPOS, &position, sizeof(BlobDemoLightPos));
}

// load light settings from engine variables
void GuiScreen::ReloadLightSettings()
{
  if (!core->SceneMan->ActiveScene)
    return;

  CSceneLights* sceneLights = core->SceneMan->ActiveScene->GetSceneLights();
  sceneLights->FirstDirectionalLight();
  if (!sceneLights->DirectionalLightEnd())
  {
    const glm::vec3& dir = sceneLights->CurrentDirectionalLight().direction;
    const glm::vec3& color = sceneLights->CurrentDirectionalLight().color;
    dlgLColour->SetRGB(color.x, color.y, color.z);
    dlgLDirection->SetValue(0, -dir.x);
    dlgLDirection->SetValue(1, -dir.z);
  }
}
