// This file is a precompiled header.
#pragma once

// if in debug mode, force tower engine to check for memory leaks
#ifdef _DEBUG
#define _DEBUG_ALLOCS 1
#endif

// set used API to OpenGL 3 (more specifically: OpenGL 3.3 Core Profile)
#define _TOWER_OPENGL_3 1
// disable warnings that standard C++ library functions may be unsafe if used wrong
#define _CRT_SECURE_NO_WARNINGS 1

// include engine
#include "../../towerengine/src/TowerEngine.h"