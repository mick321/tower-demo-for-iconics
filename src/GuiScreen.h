#pragma once
#include "_precompiled.h"
#include "Common.h"

class TowerDemoForIconics;

class GuiScreen : public CModule
{
private:
  TowerDemoForIconics  *demo;
  CColumnLayout *layout;

  // gui elements
  CButtonEx* btnExit;
  CButtonEx* btnMotion;
  CButtonEx* btnScene;
  CButtonEx* btbLColour;
  CButtonEx* btnLDirection;
  CColorDialog* dlgLColour;
  CScrollValuesDialog* dlgLDirection;
  CLabel* lblAuthors;
public:
  //constructor 
  GuiScreen(CGeneric* Owner);

  //destructor
  inline virtual ~GuiScreen(void) { Destroy(); }

  // called when engine shuts down
  void Destroy() { LibraryStop(); }

  // gui events
  int OnClick_Button(CGUIElement* elem, uint32_t flags);
  void OnColorChange(CColorDialog* elem);
  void OnDirectionChange(CScrollValuesDialog* elem, int index, float value);

  // load light settings from engine variables
  void ReloadLightSettings();
};