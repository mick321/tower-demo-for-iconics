#include "_precompiled.h"
#include "TowerDemoForIconics.h"
#include "CameraController.h"
#include "GuiScreen.h"
#pragma hdrstop

// Engine event hook, called right after engine initialization.
void TowerDemoForIconics::OnCreate()
{
  imgBlack = new CImageButton(SceneMan->GUI, rectf(OPAQUEWHITERECT_U1, OPAQUEWHITERECT_V1,
    OPAQUEWHITERECT_U1 + OPAQUEWHITERECT_PARTS_W, OPAQUEWHITERECT_V1 + OPAQUEWHITERECT_PARTS_H));
  gui = new GuiScreen(this);
  cameraController = new CameraController(this, glm::vec3(64.f, 2.f, 64.f));

  //static_cast<CTowerCORE*>(GetCore())->InputMan->LockMouse();

  // Become a "library visitor", get access to automatically managed resources.
  // Logically, not every object needs to access global library (it would be an unnecessary overhead).
  resourceHolder.LibraryStart();

  fadeout = new FadeoutData();
 
  imgBlack->Top(0);
  imgBlack->Left(0);
  imgBlack->Width(2.f, true);
  imgBlack->Height(2.f, true);
  imgBlack->Visible(false);

  // prepare scene
  LoadAllResources();
  NextScene();
  //CameraFreeMode();  // to be changed: AutoMode first
  CameraAutoMode();

  //CSpline1DEditor* edi = new CSpline1DEditor(SceneMan->GUI);
  //edi->SetWorkingRange(rectf(0.0f, 0.f, 100.f, 100.f));
}

// Engine event hook, called before engine deinitialization.
void TowerDemoForIconics::OnDestroy()
{
  // Stop using library, give back all borrowed stuff.
  resourceHolder.LibraryStop();
}

// Load all resources needed for demo.
void TowerDemoForIconics::LoadAllResources()
{
  // "prefetch" resources - not really needed, but prevents framerate stalls when resources are needed
  GlobalLibrary->Fetch<CTextureCubeMap>(&resourceHolder, "data/tex/bright/bright.cubemap");
  GlobalLibrary->Fetch<CTextureCubeMap>(&resourceHolder, "data/tex/sky-flat/sky-flat.cubemap");
  GlobalLibrary->Fetch<CTexture>(&resourceHolder, "data/tex/terrain/terrain_detail.png");
  GlobalLibrary->Fetch<CTexture>(&resourceHolder, "data/tex/terrain/terrain_color.jpg");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/stone.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/sheep.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/treesmall.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree1.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree2.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree3.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree4.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree5.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/Panelak/panelak_chodba_engprez.tme");
  GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/Panelak/panelak_vytah_modul.tme");
}

// Prepare meadow scene.
void TowerDemoForIconics::PrepareMeadow()
{
  currentScene = sceneMeadow;
  const int terrainSize = 128;
  if (SceneMan->ActiveScene == NULL)
    return;

  // delete all objects of current scene, "false" means keep cameras
  SceneMan->ActiveScene->RemoveAllObjects(false);
  // delete all lights
  CSceneLights* lights = SceneMan->ActiveScene->GetSceneLights();
  lights->DeleteAllLights();

  // 1) create new scene lights
  lights->SetAmbientLightColor(glm::vec3(0.35f, 0.35f, 0.35f));
  // first (main) directional light
  TDirectionalLight dirLight;
  dirLight.direction = defaultLightDir;
  dirLight.color = defaultLightColor;
  dirLight.intensity = 1.f;
  int lightId = lights->AddDirectionalLight(dirLight);
  {
    // set shadow map properties
    const int shadowMapRes(2048);
    lights->EnableDirectionalLightShadows(lightId, shadowMapRes);
    CDirectionalLightShadowInfo* shadowInfo = lights->FetchShadowInfo(0);
    if (shadowInfo)
    {
      shadowInfo->projectionPlaneCenter = glm::vec3(terrainSize * 0.5f, 5.f, terrainSize * 0.5f);
      shadowInfo->projectionPlaneHalfU = glm::vec3(terrainSize * 0.35f, 0, 0);
      shadowInfo->projectionPlaneHalfV = glm::vec3(0, 0, terrainSize * 0.35f);
      shadowInfo->projectionPlaneDist = 25.f;
    }
  }
  // second (helper) directional light
  dirLight = TDirectionalLight();
  dirLight.direction = glm::vec3(-0.2f, 1.0f, -0.2f);
  dirLight.color = glm::vec3(1.f, 1.f, 0.95f);
  dirLight.intensity = 0.2f;
  lights->AddDirectionalLight(dirLight);

  // 2) create sky
  auto sky = SceneMan->ActiveScene->GetSceneSky();
  auto skyCubeMap = GlobalLibrary->Fetch<CTextureCubeMap>(&resourceHolder, "data/tex/bright/bright.cubemap");
  if (skyCubeMap)
  {
    sky->SetTextureCubeMap(skyCubeMap);
    sky->Enabled(true);
  }
  else
  {
    sky->Enabled(false);
  }

  // 3) terrain
  // fetch terrain from global library
  CHandle<CTerrain> terrainData;
  if (GlobalLibrary->Contains("#meadow_terraindata"))
    terrainData = GlobalLibrary->Fetch<CTerrain>(&resourceHolder, "#meadow_terraindata"); // try loading previously stored data

  if (!terrainData) // terrainData was not found, we will generate it and store it
  {
    // load textures from library, library will auto load resources if they are not present in memory yet
    auto texDetail = GlobalLibrary->Fetch<CTexture>(&resourceHolder, "data/tex/terrain/terrain_detail.png");
    auto texColor = GlobalLibrary->Fetch<CTexture>(&resourceHolder, "data/tex/terrain/terrain_color.jpg");
    // check if everything is right
    if (texDetail && texColor)
    {
      // construct terrain data
      terrainData = CHandleManager::CreateHandle(new CTerrain(SceneMan), nullptr);
      terrainData->SetSize(terrainSize, terrainSize);
      terrainData->SetTextures(texDetail, texColor);
      terrainData->TextureTileSize = 2.f;
      terrainData->AutoDeleteTextures = false;
      // materials that look pretty okay...
      terrainData->Material.Ambient = glm::vec4(1.2f, 1.2f, 1.8f, 0.66667f) * 1.5f;
      terrainData->Material.Diffuse = glm::vec4(2.1f, 2.1f, 2.0f, 2.f) * 0.6f;
      // generate elevations
      // pretty seeds: (13222.2445f, 1320.74574f)
      CEvalTerrainGen genFunc = CEvalTerrainGen(7, terrainSize, terrainSize, glm::vec2(-2.2445f, -13.74574f));
      genFunc.Amplitude = 10.5f;
      terrainData->Generate(CEvalTerrainGen(genFunc));
      // store pointer to global library (it will be managed automatically from now)
      GlobalLibrary->StoreAndFetch(&resourceHolder, "#meadow_terraindata", terrainData);
    }
  }

  // sets camera position limitations to terrain
  cameraController->CameraLimitation(true);

  // create scene object and link it to terrain data
  auto objTerrain = SceneMan->ActiveScene->CreateObject();
  objTerrain->SetGraphicsData(terrainData);

  // 4) place trees on scene
  PrepareMeadowTrees(glm::vec2(terrainSize * 0.5f, terrainSize * 0.5f), terrainSize * 0.45f * 0.5f, terrainSize * 0.85f * 0.5f, terrainData.GetRawPointer());

  // 5) place some stones
  auto stoneMesh = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/stone.tme");
  const int stoneCount = 5;
  utils::Random stoneRandom(1535131); // deterministic pseudorandom generator
  for (int i = 0; i < stoneCount; i++)
  {
    const glm::vec2 pos = glm::vec2(terrainSize * 0.5f, terrainSize * 0.5f) + stoneRandom.randCircle(terrainSize * 0.4f * 0.5f);
    const float stoneScale = stoneRandom.randFloat(1.f, 4.f);

    auto objStone = SceneMan->ActiveScene->CreateObject();
    objStone->Translate(pos.x, terrainData->GetElevation(pos.x, pos.y), pos.y);
    objStone->RotY(stoneRandom.randFloat(0.f, 360.f));
    objStone->Scale(stoneScale, stoneScale, stoneScale);
    objStone->SetGraphicsData(stoneMesh);
    objStone->ShadowCast(true);
    objStone->ShadowReceive(true);
  }

  // 6) place some flowers
  CreateFlowers(80.f, 80.f);
  CreateFlowers(74.f, 84.f);
  CreateFlowers(50.f, 68.f);
  CreateFlowers(60.f, 40.f);

  // 7) place particles (fog)
  // load stored settings
  CParticleSettings particleSettings(this);
  TMemoryStream particleSettingsFile("data/particles/fog.tpe");
  const int particleEmitterCount = 30;
  utils::Random particleSystemRandom(129301014);
  for (int i = 0; i < particleEmitterCount; i++)
  {
    // particle emitter = specialized scene object
    auto emitterFog = SceneMan->ActiveScene->CreateObject<CParticleEmitter>();
    // particle system for our emitter, emitter will "take care" of this system (its disposing, too)
    CParticleSystem* particleSystem = emitterFog->ParticleSystem();
    particleSettingsFile.Seek(0, soBeginning);
    particleSettings.Load(particleSettingsFile, emitterFog, &resourceHolder);
    // place it on terrain
    glm::vec3 emitterPos(terrainSize * 0.5f + terrainSize * 0.28f * glm::cos(i * 2.f * glm::pi<float>() / particleEmitterCount), 
      5.5f + particleSystemRandom.randFloat(-1.f, 2.f),
      terrainSize * 0.5f + terrainSize * 0.28f * glm::sin(i * 2.f * glm::pi<float>() / particleEmitterCount));
    emitterPos.y += terrainData->GetElevation(emitterPos.x, emitterPos.z);
    emitterFog->Translate(emitterPos.x, emitterPos.y, emitterPos.z);
    // activate system
    particleSystem->Regenerate();
  }

  // 8) place particles (butterflies)
  particleSettingsFile.LoadFromFile("data/particles/butterflies.tpe");
  // particle emitter = specialized scene object
  auto emitterButterflies = SceneMan->ActiveScene->CreateObject<CParticleEmitter>();
  // particle system for our emitter, emitter will "take care" of this system (its disposing, too)
  CParticleSystem* particleSystem = emitterButterflies->ParticleSystem();
  particleSettingsFile.Seek(0, soBeginning);
  particleSettings.Load(particleSettingsFile, emitterButterflies, &resourceHolder);
  // place it on terrain
  glm::vec3 emitterPos(80.f, 0.1f, 80.f);
  emitterButterflies->Translate(emitterPos.x, emitterPos.y, emitterPos.z);
  // activate system
  particleSystem->Regenerate();
  
  emitterButterflies = SceneMan->ActiveScene->CreateObject<CParticleEmitter>();
  particleSystem = emitterButterflies->ParticleSystem();
  particleSettingsFile.Seek(0, soBeginning);
  particleSettings.Load(particleSettingsFile, emitterButterflies, &resourceHolder);
  // place it on terrain
  emitterPos = glm::vec3(74.f, 0.1f, 84.f);
  emitterButterflies->Translate(emitterPos.x, emitterPos.y, emitterPos.z);
  // activate system
  particleSystem->Regenerate();

  // 9) place sheep
  const int sheepCount = 10;
  utils::Random sheepRandom(145307787);
  utils::Random sheepRandom2(15331531);
  for (int i = 0; i < sheepCount; i++)
  {
    glm::vec3 sheepPos(terrainSize * 0.4f, 0.f, terrainSize * 0.6f);
    glm::vec2 offset(sheepRandom.randDisk(6.f));
    sheepPos.x += offset.x;
    sheepPos.z += offset.y;
    sheepPos.y += terrainData->GetElevation(sheepPos.x, sheepPos.z);

    auto sheepMesh = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/sheep.tme");
    auto sheepActor = SceneMan->ActiveScene->CreateObject<CActor>();
    sheepActor->Translate(sheepPos.x, sheepPos.y, sheepPos.z);
    sheepActor->RotY(sheepRandom2.randFloat(20.f, 160.f));
    sheepActor->SetGraphicsData(sheepMesh);
    sheepActor->SetAnimation(i % 4 == 0 ? "Stani" : "Paseni");
    sheepActor->SetAnimationSpeed(sheepRandom2.randFloat(0.6f, 1.1f));
    sheepActor->ShadowCast(true);
    sheepActor->ShadowReceive(true);
  }

  // 10) place ram
  glm::vec3 ramPos(49.5f, 0.f, 58.5f);
  ramPos.y += terrainData->GetElevation(ramPos.x, ramPos.z);

  auto ramMesh = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/ram.tme");
  auto ramActor = SceneMan->ActiveScene->CreateObject<CActor>();
  ramActor->Translate(ramPos.x, ramPos.y, ramPos.z);
  ramActor->RotY(-90.f);
  ramActor->SetGraphicsData(ramMesh);
  ramActor->SetAnimation("Idle");
  ramActor->ShadowCast(true);
  ramActor->ShadowReceive(true);
}

// Prepare elevator scene.
void TowerDemoForIconics::PrepareElevator()
{
  currentScene = sceneElevator;
  if (SceneMan->ActiveScene == NULL)
    return;

  glm::vec3 position(61.f, -1.f, 61.f);

  // delete all objects of current scene, "false" means keep cameras
  SceneMan->ActiveScene->RemoveAllObjects(false);
  // delete all lights
  CSceneLights* lights = SceneMan->ActiveScene->GetSceneLights();
  lights->DeleteAllLights();

  lights->SetAmbientLightColor(glm::vec3(0.6f, 0.6f, 0.6f));
  // first (main) directional light
  TDirectionalLight dirLight;
  dirLight.direction = glm::vec3(1.f, -0.5f, 0.5f);
  dirLight.color = defaultLightColor;
  dirLight.intensity = 0.5f;
  lights->AddDirectionalLight(dirLight);

  // skybox
  auto sky = SceneMan->ActiveScene->GetSceneSky();
  auto skyCubeMap = GlobalLibrary->Fetch<CTextureCubeMap>(&resourceHolder, "data/tex/sky-flat/sky-flat.cubemap");
  if (skyCubeMap)
  {
    sky->SetTextureCubeMap(skyCubeMap);
    sky->Enabled(true);
  }
  else
  {
    sky->Enabled(false);
  }

  auto flatHouseMesh = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/Panelak/panelak_chodba_engprez.tme");
  auto flatHouse = SceneMan->ActiveScene->CreateObject();
  flatHouse->Identity();
  flatHouse->Translate(position.x, position.y, position.z);
  flatHouse->Scale(1.f, 1.f, 1.f);
  flatHouse->SetGraphicsData(flatHouseMesh);

  position.x += 0.5f;
  position.y += 0.925f;
  position.z += 0.5f;
  auto elevatorMesh = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/Panelak/panelak_vytah_modul.tme");
  auto elevator = SceneMan->ActiveScene->CreateObject();
  elevator->Identity();
  elevator->Translate(position.x, position.y, position.z);
  elevator->Scale(1.f, 1.f, 1.f);
  elevator->SetGraphicsData(elevatorMesh);

  // prepare elevator animation (composite of down and up movement)
  auto animElevator = CAnimationChain::Create(elevator);
  // move down
  const CAnimationElement animElevatorDownElem[] =
  {
    CAnimationElement::Translate(
      glm::vec3(position.x, position.y + 5.f, position.z), 
      glm::vec3(position.x, position.y - 10.f, position.z)).SetInterpolation(CAnimationElement::interSmooth)
  };
  auto animElevatorDown = CAnimationAbsolute::Create(animElevator);  // add to chain
  animElevatorDown->Set(8 * 1000, 1, animElevatorDownElem);
  // move up
  const CAnimationElement animElevatorUpElem[] =
  {
    CAnimationElement::Translate(
      glm::vec3(position.x, position.y - 10.f, position.z),
      glm::vec3(position.x, position.y + 5.f, position.z)).SetInterpolation(CAnimationElement::interSmooth)
  };
  auto animElevatorUp = CAnimationAbsolute::Create(animElevator);  // add to chain
  animElevatorUp->Set(8 * 1000, 1, animElevatorUpElem);
  // wait
  const CAnimationElement animElevatorWaitElem[] =
  {
    CAnimationElement::Translate(
    glm::vec3(position.x, position.y + 5.f, position.z),
    glm::vec3(position.x, position.y + 5.f, position.z))
  };
  auto animElevatorWait = CAnimationAbsolute::Create(animElevator);  // add to chain
  animElevatorWait->Set(2 * 1000, 1, animElevatorWaitElem);
  // setup chain and start animation
  animElevator->Set();
  animElevator->Start(true);
}

// Prepare trees on meadow scene.
void TowerDemoForIconics::PrepareMeadowTrees(const glm::vec2& centerPoint, const float minR, const float maxR, const CTerrain* terrainData)
{
  CScene* scene = ((CTowerCORE*)GetCore())->SceneMan->ActiveScene;
  if (!scene || !terrainData)
    return;

  // fetch models from global library
  const int MODEL_COUNT = 6;
  CHandle<CMesh> treeMesh[MODEL_COUNT];
  float size[MODEL_COUNT];
  const float meshScale = 1.5f;
  treeMesh[0] = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/treesmall.tme");
  treeMesh[1] = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree1.tme");
  treeMesh[2] = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree2.tme");
  treeMesh[3] = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree3.tme");
  treeMesh[4] = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree4.tme");
  treeMesh[5] = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/tree5.tme");
  // compute approximate size of models
  for (int i = 0; i < MODEL_COUNT; i++)
    size[i] = glm::max(treeMesh[i]->BBAllVerts.CoordMax.x - treeMesh[i]->BBAllVerts.CoordMin.x, 
      treeMesh[i]->BBAllVerts.CoordMax.z - treeMesh[i]->BBAllVerts.CoordMin.z) * 0.5f * 1.2f;

  // place trees on terrain - this algorithm is more complex, we just don't want our trees intersect...
  PointsFi points(minR, maxR); // helper object for placing objects in ring
  utils::Random treeRandom(456841); // deterministic pseudorandom number generator
  PointsFi::TPlacingResult res;
  float thresholdSmallTree = glm::lerp(minR, maxR, 0.1f);
  float thresholdBigTree = glm::lerp(minR, maxR, 0.8f);
  do
  {
    int modeli = treeRandom.randInt() % MODEL_COUNT;
    float scale = scale = glm::linearRand(1.f, 1.25f) * meshScale;

    float r, fi;
    res = points.placeObject(size[modeli] * scale, r, fi);
    if (res == PointsFi::presOk)
    {
      // seems all right to put tree on terrain
      auto obj = scene->CreateObject();
      obj->ShadowCast(r < glm::lerp(minR, maxR, 0.35f)); // distant trees don't have to cast shadow
      obj->ShadowReceive(r < glm::lerp(minR, maxR, 0.35f));
      glm::vec2 pos = centerPoint + r * glm::vec2(cos(fi), sin(fi));
      float yoffset = treeRandom.randFloat(-0.25f * scale, 0.f);
      obj->Translate(pos.x, terrainData->GetElevation(pos.x, pos.y) + yoffset, pos.y);
      obj->RotYRads(treeRandom.randFloat(0.f, 2.f * glm::pi<float>()));
      if (r < thresholdSmallTree)
      {
        obj->Scale(scale, scale, scale);
        obj->SetGraphicsData(treeMesh[0]);
      }
      else if (r > thresholdBigTree)
      {
        obj->Scale(scale, scale, scale);
        obj->SetGraphicsData((rand() % 2) ? treeMesh[MODEL_COUNT - 1] : treeMesh[MODEL_COUNT - 2]);
      }
      else
      {
        obj->Scale(scale, scale * treeRandom.randFloat(1.f, 1.25f), scale);
        obj->SetGraphicsData(treeMesh[modeli]);
      }
    }
  } while (res != PointsFi::presFailEnd);

  float xOffset = 10.f;
  float zOffset = 1.f;

  auto obj = scene->CreateObject();
  obj->ShadowCast(true);
  obj->ShadowReceive(true);
  obj->Translate(55.f + xOffset, terrainData->GetElevation(55.f + xOffset, 55.f + zOffset), 55.f + zOffset);
  obj->RotYRads(0.f);
  obj->Scale(1.3f, 1.3f, 1.3f);
  obj->SetGraphicsData(treeMesh[0]);

  obj = scene->CreateObject();
  obj->ShadowCast(true);
  obj->ShadowReceive(true);
  obj->Translate(58.f + xOffset, terrainData->GetElevation(58.f + xOffset, 56.f + zOffset), 56.f + zOffset);
  obj->RotYRads(1.6f);
  obj->Scale(1.3f, 1.3f, 1.3f);
  obj->SetGraphicsData(treeMesh[4]);

  obj = scene->CreateObject();
  obj->ShadowCast(true);
  obj->ShadowReceive(true);
  obj->Translate(56.f + xOffset, terrainData->GetElevation(56.f + xOffset, 52.f + zOffset), 52.f + zOffset);
  obj->RotYRads(2.1f);
  obj->Scale(1.0f, 1.0f, 1.0f);
  obj->SetGraphicsData(treeMesh[5]);
}

// Creates flowers on terrain at given coordinates
void TowerDemoForIconics::CreateFlowers(float x, float z)
{
  CHandle<CTerrain> terrainData;
  if (GlobalLibrary->Contains("#meadow_terraindata"))
    terrainData = GlobalLibrary->Fetch<CTerrain>(&resourceHolder, "#meadow_terraindata");
  else
    return;

  auto flowersMesh = GlobalLibrary->Fetch<CMesh>(&resourceHolder, "data/mesh/flowers.tme");
  glm::vec3 posFlowers(x, terrainData->GetElevation(x, z) + 0.05f, z);

  auto objFlowers = SceneMan->ActiveScene->CreateObject();
  objFlowers->Translate(posFlowers.x, posFlowers.y, posFlowers.z);
  objFlowers->LocalMatrix = objFlowers->LocalMatrix * terrainData->GetOrientationPoint((int)x, (int)z);
  objFlowers->RotYRads(glm::linearRand(0.f, glm::pi<float>()));
  objFlowers->SetGraphicsData(flowersMesh);
  objFlowers->ShadowCast(false);
  objFlowers->ShadowReceive(true);
}

// Engine event hook, called on every frame start.
void TowerDemoForIconics::OnEveryFrameStart()
{
  if (TowerApp->Inputs.Keyboard.keys[SDL_SCANCODE_ESCAPE] != TButtonState::Up)
  {
    if (cameraMode == cameraNothing)
      CameraFreeMode();
    else 
      CameraStop();
    TowerApp->Inputs.Keyboard.keys[SDL_SCANCODE_ESCAPE] = TButtonState::Up;
  }
  
  imgBlack->Visible(true);
  switch (fadeout->fading) {
    case FadeoutData::fadeOut: 
      fadeout->alpha += 0.1f * TimeMan->GetMainTime()->GetDiff() * 0.005f;
      if (fadeout->alpha >= 1.f)
      {
        fadeout->alpha = 1.f;
        fadeout->fading = FadeoutData::fadeNothing;
        NextScene();
      }
      break;
    case FadeoutData::fadeIn: 
      fadeout->alpha -= 0.1f * TimeMan->GetMainTime()->GetDiff() * 0.005f;
      if (fadeout->alpha <= 0.f)
      {
        fadeout->alpha = 0.f;
        fadeout->fading = FadeoutData::fadeNothing;
      }
      break;
    default: imgBlack->Visible(false);
  }

  cameraController->SetupCamera(cameraMode, currentScene);
  imgBlack->SetColor(0,0,0, fadeout->alpha);
}

// Engine event hook, called when debugging drawing is possible.
void TowerDemoForIconics::OnPossibleSimpleDraw()
{
  CSimpleDraw* sd = SceneMan->SimpleDraw;  
  (void)sd;
}

// Switch to next available scene.
void TowerDemoForIconics::NextScene()
{
  currentScene = (TSceneIdentifier)(currentScene + 1);
  if (currentScene == sceneCountOfElements)
    currentScene = sceneMeadow;

  switch (currentScene)
  {
  case sceneMeadow:
    PrepareMeadow();
    break;
  case sceneElevator:
    PrepareElevator();
    cameraController->CameraLimitation(false);
    break;
  }

  cameraController->PreparePresentation(currentScene);
  cameraController->ResetInitialPosition(currentScene);
  fadeout->fading = FadeoutData::fadeIn;
  gui->ReloadLightSettings();
}

// Camera in free mode.
void TowerDemoForIconics::CameraFreeMode()
{
  cameraMode = cameraFree;
  InputMan->LockMouse();
}

// Camera in automatic mode.
void TowerDemoForIconics::CameraAutoMode()
{
  cameraMode = cameraAuto;
}

// Camera stopped and cursor is not bound to center of screen anymore.
void TowerDemoForIconics::CameraStop()
{
  cameraMode = cameraNothing;
  InputMan->UnlockMouse(true);
}

// Set direction of main light.
void TowerDemoForIconics::SetMainLightDirection(const glm::vec3& lightDirection)
{
  if (!SceneMan->ActiveScene)
    return;

  CSceneLights* sceneLights = SceneMan->ActiveScene->GetSceneLights();
  sceneLights->FirstDirectionalLight();
  if (!sceneLights->DirectionalLightEnd())
    sceneLights->CurrentDirectionalLight().direction = glm::normalize(lightDirection);
}

// Set color of main light.
void TowerDemoForIconics::SetMainLightColor(const glm::vec3& lightColor)
{
  if (!SceneMan->ActiveScene)
    return;

  CSceneLights* sceneLights = SceneMan->ActiveScene->GetSceneLights();
  sceneLights->FirstDirectionalLight();
  if (!sceneLights->DirectionalLightEnd())
    sceneLights->CurrentDirectionalLight().color = lightColor;
}

// Processing universal engine messages.
bool TowerDemoForIconics::ProcessMessageEx(uint32_t msg, void* blob, uint32_t blobsize, uint32_t flags)
{
  switch (msg)
  {
  case MSG_DEMO_LIGHTPOS:
    {
      glm::vec3 lightDirection = -reinterpret_cast<BlobDemoLightPos*>(blob)->position;
      lightDirection.y = defaultLightDir.y;
      SetMainLightDirection(lightDirection);
    }
    return true;
  case MSG_DEMO_LIGHTCOLOR:
    SetMainLightColor(reinterpret_cast<BlobDemoLightColor*>(blob)->color);
    return true;
  case MSG_DEMO_CAMERA:
    if (cameraMode == cameraNothing || cameraMode == cameraFree)
      CameraAutoMode();
    else
      CameraFreeMode();
    return true;
  case MSG_DEMO_NEXTSCENE:
    fadeout->fading = FadeoutData::fadeOut;
    return true;
  case MSG_DEMO_EXIT:
    Finish();
    return true;
  }
  return CTowerCORE::ProcessMessageEx(msg, blob, blobsize, flags);
}