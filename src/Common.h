#pragma once
#include "_precompiled.h"

#define MSG_DEMO_NEXTSCENE     100001
#define MSG_DEMO_EXIT          100002
#define MSG_DEMO_CAMERA        100003
#define MSG_DEMO_LIGHTPOS      100004
struct BlobDemoLightPos
{
  glm::vec3 position;  //!< x, z will be used
};
#define MSG_DEMO_LIGHTCOLOR    100005
struct BlobDemoLightColor
{
  glm::vec3 color;  //!< x, y, z = r, g, b
};

struct FadeoutData
{
  enum { fadeNothing, fadeOut, fadeIn } fading;
  float alpha;

  inline FadeoutData() : fading(fadeNothing), alpha(0.f) { }
};
// Enumeration of possible scenes.
enum TSceneIdentifier
{
  sceneUnknown = 0,
  sceneMeadow,
  sceneElevator,
  sceneCountOfElements
};

// Enumeration of possible camera modes.
enum TCameraMode
{
  cameraNothing = 0,
  cameraFree,
  cameraAuto
};

static const glm::vec3 defaultLightDir(0.5f, -0.75f, 0.5f);
static const glm::vec3 defaultLightColor(0.7f, 0.7f, 0.8f);