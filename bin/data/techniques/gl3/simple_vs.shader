#version 330
// vertex shader for simple drawing

in vec4 position;
in vec4 color;

uniform mat4 g_world;
uniform mat4 g_viewproj;

out inter {
  vec4 v_color;
} Out;

void main()
{
  vec4 pos = position;
  pos.w = 1.0;
  gl_Position = g_viewproj * g_world * pos;  
  Out.v_color = color;
}
