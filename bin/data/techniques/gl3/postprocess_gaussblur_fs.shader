#version 330
// fragment shader for postprocess blur

uniform sampler2D tex;
uniform vec4 g_direction;

in inter {
  vec2 v_uv;
} In;

out vec4 fragmentColor;

const float gauss_fact[] = float[7](4,12,20,25,20,12,4);
const float dir_fact[] = float[7](-3,-2,-1,0,1,2,3);
const float gauss_sum = 97;

void main()
{
  vec4 color = vec4(0);
  for (int i = 0; i < 7; i++)
    color += (gauss_fact[i]/gauss_sum) * texture(tex, In.v_uv + g_direction.xy * dir_fact[i]);
  fragmentColor = color;
}