#version 330
// fragment shader for postprocessing

uniform sampler2D tex;
uniform sampler2D texGlowColor;

uniform vec4 g_colorcorrection;
uniform vec4 g_screenresolution;

in inter {
  vec2 v_uv;
} In;

out vec4 fragmentColor;  

const vec3 lumCoeff = vec3(0.2125, 0.7154, 0.0721);
#ifdef##EDGEDETECT
const vec4 onezerovec = vec4(1, -1, 0.0, 0.0);
const vec4 twozerovec = vec4(2, -2, 0.0, 0.0);
#endif

void main()
{
  #ifdef##LENS
  float coeff = length(abs(In.v_uv - vec2(0.5, 0.5))) / length(vec2(0.5,0.5));
  vec2 uv = mix(vec2(0.5, 0.5), In.v_uv, coeff);
  #else
  vec2 uv = In.v_uv;
  #endif
  
  #ifdef##EDGEDETECT
  vec4 original = texture(tex, uv);
  vec4 edges = 12 * original;
  edges -= 2*texture(tex, uv + g_screenresolution.zw * onezerovec.yw); // left
  edges -= 2*texture(tex, uv + g_screenresolution.zw * onezerovec.xw); // right
  edges -= 2*texture(tex, uv + g_screenresolution.zw * onezerovec.wx); // up
  edges -= 2*texture(tex, uv + g_screenresolution.zw * onezerovec.wy); // down
  edges -= texture(tex, uv + g_screenresolution.zw * twozerovec.yw); // left
  edges -= texture(tex, uv + g_screenresolution.zw * twozerovec.xw); // right
  edges -= texture(tex, uv + g_screenresolution.zw * twozerovec.wx); // up
  edges -= texture(tex, uv + g_screenresolution.zw * twozerovec.wy); // down
  edges.xyz = vec3(1 - smoothstep(0.05, 0.5, dot(edges.xyz, vec3(1,1,1))));
  edges.w = 1.0;
  original = original * edges + texture(texGlowColor, uv); // add glow after applying edges
  #else
  vec4 original = texture(tex, uv);
  original += texture(texGlowColor, uv);
  #endif
  
  // brightness
  vec3 color = original.xyz * g_colorcorrection.x;
  // contrast
  color = mix(vec3(0.5), color, g_colorcorrection.y);
  vec3 intensity = vec3(dot(lumCoeff, color));
  // saturation, extrapolation (negative parameter) causes complementary colors
  color = mix(intensity, color, g_colorcorrection.z);
  fragmentColor = vec4(color, original.w);
}