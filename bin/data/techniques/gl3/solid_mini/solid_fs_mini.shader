#version 330
// fragment shader for drawing solid objects

#include "phong.shaderpart"

    // global constants
	layout (std140) uniform GlobalBlock {
		mat4 g_viewproj;
		vec4 g_cameraposition;
	};
	
	// local constants
	const int g_lightmaxcount = ##MAX_LIGHT_COUNT;
    uniform mat4 g_world;
    uniform mat4 g_worldnormals;
    uniform vec4 g_lightambient;
    uniform vec4 g_lightoptions;
    uniform vec4 g_lightvecarray[g_lightmaxcount];
    uniform vec4 g_lightcolorarray[g_lightmaxcount];
    uniform vec4 g_lightoptionsarray[g_lightmaxcount];
    uniform vec4 g_matambient;
    uniform vec4 g_matdiffuse;
    uniform vec4 g_matspecular;
#ifdef##USE_TEXARR
    uniform vec4 g_mattextures;
#endif


uniform sampler2D texDiffuse;
#ifdef##SKY_LIGHTING
uniform samplerCube texSkyDiffuse;
#endif
#ifdef##SKY_LIGHTING_REFLECT
uniform samplerCube texSkySpecular;
#endif
#ifdef##USE_TEXARR
uniform sampler2D texSpecularMap;
#ifdef##NORMALMAP
uniform sampler2D texNormalMap;
#endif
#endif

in inter {
  vec4 v_world_pos;
#ifdef##NORMALMAP
  mat3 mat_tbn;
#else
  vec3 v_world_normal;
#endif
  vec2 v_texcoord;
} In;

out vec4 fragmentColor;

void main()
{
  fragmentColor = vec4(1);
}