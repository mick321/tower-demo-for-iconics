#version 330
// vertex shader for drawing solid objects

in vec3 position;
in vec3 normal;
in vec2 uv;
#ifdef##NORMALMAP
in vec3 tangent;
#endif

// global constants
layout (std140) uniform GlobalBlock {
    mat4 g_viewproj;
    vec4 g_cameraposition;
    
    mat4 g_shadowmatrix;
    vec4 g_shadowoptions;
};

// local constants
    const int g_lightmaxcount = ##MAX_LIGHT_COUNT;
    uniform mat4 g_world;
    uniform mat4 g_worldnormals;
    uniform vec4 g_lightambient;
    uniform vec4 g_lightoptions;
    uniform vec4 g_lightvecarray[g_lightmaxcount];
    uniform vec4 g_lightcolorarray[g_lightmaxcount];
    uniform vec4 g_lightoptionsarray[g_lightmaxcount];
    uniform vec4 g_matambient;
    uniform vec4 g_matdiffuse;
    uniform vec4 g_matspecular;
#ifdef##USE_TEXARR
    uniform vec4 g_mattextures;
#endif
#ifdef##TERRAIN
    uniform vec4 g_terrainsize;
#endif
    uniform vec4 g_shadowsobject;

out inter {
  vec4 v_world_pos;
#ifdef##NORMALMAP
  mat3 mat_tbn;
#else
  vec3 v_world_normal;
#endif
  vec2 v_texcoord;
#ifdef##TERRAIN
  vec2 v_texcoordTer;
#endif
} Out;

void main()
{
  const vec2 constantListOneZero = vec2(1.0, 0.0);
  vec4 pos = position.xyzx * constantListOneZero.xxxy + constantListOneZero.yyyx; /* (x,y,z,1) */
  #ifdef##TERRAIN
    Out.v_texcoordTer = position.xz * g_terrainsize.zw;
  #endif
  Out.v_world_pos = g_world * pos;
  gl_Position = g_viewproj * Out.v_world_pos;
  
  /* only works for uniform scale - fix it! */
  vec4 nor = normal.xyzx * constantListOneZero.xxxy; /* (x,y,z,0) */
#ifdef##NORMALMAP
  vec4 tangent4 = tangent.xyzx * constantListOneZero.xxxy; /* (x,y,z,0) */
  Out.mat_tbn = mat3((g_worldnormals * tangent4).xyz,
                     vec3(0),
                     (g_worldnormals * nor).xyz);
  Out.mat_tbn[1] = cross(Out.mat_tbn[0], Out.mat_tbn[2]);
#else
  Out.v_world_normal = (g_worldnormals * nor).xyz;
#endif
  Out.v_texcoord = uv;
}
