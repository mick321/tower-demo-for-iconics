#version 330
// fragment shader for drawing solid objects

#ifdef##USE_UBO

    // global constants
	layout (std140) uniform GlobalBlock {
		mat4 g_viewproj;
	};
	
	// local constants
	const int g_lightmaxcount = ##MAX_LIGHT_COUNT;
	uniform mat4 g_world;

#else
	uniform mat4 g_viewproj;
	uniform mat4 g_world;
#endif

in inter {
  vec4 v_clip_pos;
} In;

out float fragmentColor;

void main()
{
  float d = clamp(0.5f + 0.5f * (In.v_clip_pos.z / In.v_clip_pos.w), 0, 1);
  fragmentColor = d;//vec4(d, 0, 0, 0);
}