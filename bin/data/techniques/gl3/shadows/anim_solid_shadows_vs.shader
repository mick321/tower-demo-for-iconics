#version 330
// vertex shader for drawing solid objects

in vec3 position;
in vec4 boneIW;

#ifdef##USE_UBO

    // global constants
	layout (std140) uniform GlobalBlock {
		mat4 g_viewproj;
	};
	
	// local constants
	const int g_lightmaxcount = ##MAX_LIGHT_COUNT;
    uniform mat4 g_world;
	
    // bone constants
    const int g_bonearraymaxcount = ##MAX_BONE_COUNT * 3;
    uniform vec4 g_bonearray[g_bonearraymaxcount];
    
#else
	uniform mat4 g_viewproj;
	uniform mat4 g_world;
#endif

out inter {
  vec4 v_clip_pos;
} Out;

vec4 compute_skinning(vec4 objpos)
{
  vec4 rtn = vec4(0,0,0,1);
  for (int i = 0; i < 4; i++)
  {
    int index = int(boneIW[i]) * 3;
    float weight = fract(boneIW[i]) * 2;
    
    vec3 temp;
    temp.x = dot(g_bonearray[index], objpos);
    temp.y = dot(g_bonearray[index+1], objpos);
    temp.z = dot(g_bonearray[index+2], objpos);
    rtn.xyz += temp * weight;
  }
  return rtn;
}

void main()
{
  const vec2 constantListOneZero = vec2(1.0, 0.0);
  vec4 pos = position.xyzx * constantListOneZero.xxxy + constantListOneZero.yyyx; /* (x,y,z,1) */
  gl_Position = Out.v_clip_pos = g_viewproj * (g_world * compute_skinning(pos));
}
