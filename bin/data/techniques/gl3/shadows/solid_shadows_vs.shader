#version 330
// vertex shader for drawing shadows of solid objects

in vec3 position;

#ifdef##USE_UBO

    // global constants
	layout (std140) uniform GlobalBlock {
		mat4 g_viewproj;
	};
	
	// local constants
	const int g_lightmaxcount = ##MAX_LIGHT_COUNT;
	uniform mat4 g_world;

#else
	uniform mat4 g_viewproj;
	uniform mat4 g_world;
#endif

out inter {
  vec4 v_clip_pos;
} Out;

void main()
{
  const vec2 constantListOneZero = vec2(1.0, 0.0);
  vec4 pos = position.xyzx * constantListOneZero.xxxy + constantListOneZero.yyyx; /* (x,y,z,1) */
  gl_Position = Out.v_clip_pos = g_viewproj * (g_world * pos);
}
