#version 330
// fragment shader for drawing solid objects

#include "phong.shaderpart"

// global constants
layout (std140) uniform GlobalBlock {
    mat4 g_viewproj;
    vec4 g_cameraposition;
};

// local constants
const int g_lightmaxcount = ##MAX_LIGHT_COUNT;
uniform mat4 g_world;
uniform mat4 g_worldnormals;

#ifdef##NORMALMAP
uniform sampler2D texNormalMap;
#endif

in inter {
  vec4 v_world_pos;
#ifdef##NORMALMAP
  mat3 mat_tbn;
#else
  vec3 v_world_normal;
#endif
  vec2 v_texcoord;
} In;

out vec4 fragmentColor;

void main()
{
  #ifdef##NORMALMAP
  vec3 n_normal = In.mat_tbn * (2*texture(texNormalMap, In.v_texcoord).xyz-vec3(1));
  n_normal = normalize(n_normal);
  #else
  vec3 n_normal = normalize(In.v_world_normal);
  #endif
  fragmentColor = vecToColor(n_normal);
}