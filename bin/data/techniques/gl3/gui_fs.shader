#version 330
// fragment shader for simple drawing

uniform sampler2D texGui;
uniform sampler2D texFont;

in inter {
  vec4 v_color;
  vec2 v_uv;
  vec2 v_blend;
} In;

out vec4 fragmentColor;  

void main()
{
  vec4 res;
  res = In.v_blend.x * texture(texGui, In.v_uv);
  res += In.v_blend.y * texture(texFont, In.v_uv);
  fragmentColor = In.v_color * res;
}