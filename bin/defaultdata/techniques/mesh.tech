technique mesh extends "_base.tech"
# ----------- api blocks -----------------------------------
api gl3 {
  # ------- static case ------------------------------------
  case static {
    default
    define MATERIAL ""
	define LIGHTING ""
    define USE_TEXARR ""
  
    pass {
      vshader "gl3/solid_vs.shader"
      fshader "gl3/solid_fs.shader"
    }
    
    constants {
      clear
      block GlobalBlock {
	    matrix g_viewproj from global c_matrixviewproj
        float4 g_cameraposition from global c_cameraposition
        matrix g_shadowmatrix from global c_shadowmatrix
        float4 g_shadowoptions from global c_shadowoptions
      }
      
      matrix g_world from local c_matrixworld
      matrix g_worldnormals from local c_matrixworldnormals
      
      float4 g_matambient from local c_materialambient
      float4 g_matdiffuse from local c_materialdiffuse
      float4 g_matspecular from local c_materialspecular
      
      float4 g_mattextures from local c_materialtextures
      
      float4 g_lightambient from local c_lightambientcolor
      float4 g_lightoptions from local c_lightoptions
      float4 g_lightvecarray{8} from local c_lightvecarray
      float4 g_lightoptionsarray{8} from local c_lightoptionsarray
      float4 g_lightcolorarray{8} from local c_lightcolorarray
      
      float4 g_shadowsobject from local c_shadowoptionsobject
    }
    
    textures {
      clear
      color texDiffuse
      gloss texSpecularMap
      shadowdepth texShadowMap
    }
    
    inputlayout {
      clear
      position {
        hint position
        stage 0
        components 3
        type float
        offset 0
        stride 44
      }
      normal {
        hint normal
        stage 0
        components 3
        type float
        offset 12
        stride 44
      }
      uv {
        hint texcoord
        stage 0
        components 2
        type float
        offset 24
        stride 44
      }
    }
  } # end of case static
  
  # ------- static-glow case ------------------------------
  case static-glow {
  
    pass {
      vshader "gl3/solid_glow_vs.shader"
      fshader "gl3/solid_glow_fs.shader"
    }
    
    constants {
      clear
      block GlobalBlock {
	    matrix g_view from global c_matrixview
        matrix g_proj from global c_matrixproj
        float4 g_nearfar from global c_cameranearfar      
      }
      
      matrix g_world from local c_matrixworld
      float4 g_matdiffuse from local c_materialdiffuse
    }
    
    textures {
      clear
      color texDiffuse
    }
    
    inputlayout {
      clear
      position {
        hint position
        stage 0
        components 3
        type float
        offset 0
        stride 44
      }
      uv {
        hint texcoord
        stage 0
        components 2
        type float
        offset 24
        stride 44
      }
    }
  } # end of case static-glow
  
  
  # ------- static-shadow case ------------------------------
  case static-shadow {
  
    pass {
      vshader "gl3/shadows/solid_shadows_vs.shader"
      fshader "gl3/shadows/solid_shadows_fs.shader"
    }
    
    constants {
      clear
      block GlobalBlock {
	    matrix g_viewproj from global c_matrixviewproj
      }
      matrix g_world from local c_matrixworld
    }
    
    textures {
      clear
    }
    
    inputlayout {
      clear
      position {
        hint position
        stage 0
        components 3
        type float
        offset 0
        stride 44
      }
    }
  } # end of case static-shadow
  
  
  # ------- animated case ------------------------------------
  case animated {
    define MATERIAL ""
	define LIGHTING ""
    define USE_TEXARR ""
  
    pass {
      vshader "gl3/anim_solid_vs.shader"
      fshader "gl3/solid_fs.shader"
    }
    
    constants {
      clear
      block GlobalBlock {
	    matrix g_viewproj from global c_matrixviewproj
        float4 g_cameraposition from global c_cameraposition
        matrix g_shadowmatrix from global c_shadowmatrix
        float4 g_shadowoptions from global c_shadowoptions
      }
      
      matrix g_world from local c_matrixworld
      matrix g_worldnormals from local c_matrixworldnormals
      
      float4 g_matambient from local c_materialambient
      float4 g_matdiffuse from local c_materialdiffuse
      float4 g_matspecular from local c_materialspecular
      
      float4 g_mattextures from local c_materialtextures
      
      float4 g_lightambient from local c_lightambientcolor
      float4 g_lightoptions from local c_lightoptions
      float4 g_lightvecarray{8} from local c_lightvecarray
      float4 g_lightoptionsarray{8} from local c_lightoptionsarray
      float4 g_lightcolorarray{8} from local c_lightcolorarray
      
      float4 g_shadowsobject from local c_shadowoptionsobject
      
      float4 g_bonearray{150} from local c_bonearray
    }
    
    textures {
      clear
      color texDiffuse
      gloss texSpecularMap
      shadowdepth texShadowMap
    }
    
    inputlayout {
      clear
      position {
        hint position
        stage 0
        components 3
        type float
        offset 0
        stride 60
      }
      normal {
        hint normal
        stage 0
        components 3
        type float
        offset 12
        stride 60
      }
      uv {
        hint texcoord
        stage 0
        components 2
        type float
        offset 24
        stride 60
      }
      boneIW {
        hint texcoord
        stage 0
        components 4
        type float
        offset 44
        stride 60
      }
    }
  } # end of case animated
  
  # ------- animated-glow case ------------------------------
  case animated-glow {
  
    pass {
      vshader "gl3/anim_solid_glow_vs.shader"
      fshader "gl3/solid_glow_fs.shader"
    }
    
    constants {
      clear
      block GlobalBlock {
	    matrix g_view from global c_matrixview
        matrix g_proj from global c_matrixproj
        float4 g_nearfar from global c_cameranearfar      
      }
      
      matrix g_world from local c_matrixworld
      float4 g_matdiffuse from local c_materialdiffuse
      float4 g_bonearray{150} from local c_bonearray
    }
    
    textures {
      clear
      color texDiffuse
    }
    
    inputlayout {
      clear
      position {
        hint position
        stage 0
        components 3
        type float
        offset 0
        stride 60
      }
      uv {
        hint texcoord
        stage 0
        components 2
        type float
        offset 24
        stride 60
      }
      boneIW {
        hint texcoord
        stage 0
        components 4
        type float
        offset 44
        stride 60
      }
    }
  } # end of case animated-glow
  
  
  # ------- static-shadow case ------------------------------
  case animated-shadow {
    
    pass {
      vshader "gl3/shadows/anim_solid_shadows_vs.shader"
      fshader "gl3/shadows/solid_shadows_fs.shader"
    }
    
    constants {
      clear
      block GlobalBlock {
	    matrix g_viewproj from global c_matrixviewproj
      }
      matrix g_world from local c_matrixworld
      float4 g_bonearray{150} from local c_bonearray
    }
    
    textures {
      clear
    }
    
    inputlayout {
      clear
      position {
        hint position
        stage 0
        components 3
        type float
        offset 0
        stride 60
      }
      boneIW {
        hint texcoord
        stage 0
        components 4
        type float
        offset 44
        stride 60
      }
    }
  } # end of case animated-shadow
  
} # end of api gl3