#version 330
// vertex shader for postprocess blur

in vec2 position;

out inter {
  vec2 v_uv;
} Out;

void main()
{
  gl_Position.x = position.x;
  gl_Position.y = position.y;
  gl_Position.z = 1.f;
  gl_Position.w = 1.f;
  Out.v_uv    = 0.5 * (position + 1);
}
