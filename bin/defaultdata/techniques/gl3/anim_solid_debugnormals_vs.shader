#version 330
// vertex shader for drawing solid objects

in vec3 position;
in vec3 normal;
in vec2 uv;
#ifdef##NORMALMAP
in vec3 tangent;
#endif
in vec4 boneIW;

// global constants
layout (std140) uniform GlobalBlock {
    mat4 g_viewproj;
    vec4 g_cameraposition;
};

// local constants
const int g_lightmaxcount = ##MAX_LIGHT_COUNT;
    uniform mat4 g_world;
    uniform mat4 g_worldnormals;

// bone constants
const int g_bonearraymaxcount = ##MAX_BONE_COUNT * 3;
    uniform vec4 g_bonearray[g_bonearraymaxcount];

out inter {
  vec4 v_world_pos;
#ifdef##NORMALMAP
  mat3 mat_tbn;
#else
  vec3 v_world_normal;
#endif
  vec2 v_texcoord;
} Out;

vec4 compute_skinning(vec4 objpos)
{
  vec4 rtn = vec4(0,0,0,1);
  for (int i = 0; i < 4; i++)
  {
    int index = int(boneIW[i]) * 3;
    float weight = fract(boneIW[i]) * 2;
    
    vec3 temp;
    temp.x = dot(g_bonearray[index], objpos);
    temp.y = dot(g_bonearray[index+1], objpos);
    temp.z = dot(g_bonearray[index+2], objpos);
    rtn.xyz += temp * weight;
  }
  return rtn;
}

void main()
{
  const vec2 constantListOneZero = vec2(1.0, 0.0);
  vec4 pos = position.xyzx * constantListOneZero.xxxy + constantListOneZero.yyyx; /* (x,y,z,1) */
  
  Out.v_world_pos = g_world * compute_skinning(pos);
  gl_Position = g_viewproj * Out.v_world_pos;
  
  vec4 nor = normal.xyzx * constantListOneZero.xxxy; /* (x,y,z,0) */
#ifdef##NORMALMAP
  vec4 tangent4 = tangent.xyzx * constantListOneZero.xxxy; /* (x,y,z,0) */
  Out.mat_tbn = mat3((g_worldnormals * tangent4).xyz,
                     vec3(0),
                     (g_worldnormals * nor).xyz);
  Out.mat_tbn[1] = cross(Out.mat_tbn[0], Out.mat_tbn[2]);
#else
  Out.v_world_normal = (g_worldnormals * compute_skinning(nor)).xyz;
#endif
  Out.v_texcoord = uv;
}
