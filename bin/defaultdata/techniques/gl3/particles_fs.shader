#version 330
// fragment shader for particle drawing

layout (std140) uniform GlobalBlock
{
  mat4 g_proj;
  mat4 g_view;
  vec4 g_nearfar;
};

uniform sampler2D texDiffuse;
uniform sampler2D texDepth;

in inter {
  vec4 v_color;
  vec2 v_texcoord;
  vec2 v_texcoord2;
  vec2 fullscreen_texcoord;
  float lerp_texcoord;
  float depth;
  float invparticlesize;
} In;

out vec4 fragmentColor; 

float linearizeDepth(float d, float zNear, float zFar)
{
  return 2.0 * zNear * zFar / (zFar + zNear - (2*d-1) * (zFar - zNear));
} 

void main()
{  
  float dstDepth = linearizeDepth(texture(texDepth, In.fullscreen_texcoord).r, g_nearfar.x, g_nearfar.y);
  vec4 color = In.v_color;
  float depthdiff = (dstDepth - In.depth);
  float behindparam = step(0.01, -depthdiff);
  color.a = mix(clamp(In.invparticlesize * pow(2*depthdiff, 1.2), 0, color.a), color.a, behindparam);
  fragmentColor = color * mix(texture(texDiffuse, In.v_texcoord), texture(texDiffuse, In.v_texcoord2), In.lerp_texcoord);
}