#version 330
// vertex shader for particle drawing

in vec4 position;
in vec4 color;
in vec4 offset_and_uv;

layout (std140) uniform GlobalBlock
{
  mat4 g_proj;
  mat4 g_view;
  vec4 g_nearfar;
};

uniform mat4 g_world;
uniform float g_framecount;

out inter {  
  vec4 v_color;
  vec2 v_texcoord;
  vec2 v_texcoord2;
  vec2 fullscreen_texcoord;
  float lerp_texcoord;
  float depth;
  float invparticlesize;
} Out;

void main()
{
  vec4 pos;
  float progress;
  pos.xyz = position.xyz;
  progress = position.w;
  pos.w = 1.0;
  
  pos = g_world * pos;
  pos = g_view * pos;
  pos.xy += offset_and_uv.xy;
  Out.invparticlesize = 2/max(abs(offset_and_uv.x), abs(offset_and_uv.y));
  
  Out.v_color = color;
  
  float progress_dot_framecount = progress * g_framecount * 0.9999f;
  
  Out.v_texcoord.x = (offset_and_uv.z + max(0.f, floor(progress_dot_framecount - 1.f))) / g_framecount;
  Out.v_texcoord2.x = (offset_and_uv.z + floor(progress_dot_framecount)) / g_framecount;
  Out.v_texcoord.y = offset_and_uv.w;
  Out.v_texcoord2.y = offset_and_uv.w;
  
  pos = g_proj * pos;
  Out.fullscreen_texcoord.xy = 0.5 * (1 + pos.xy / pos.w);
  Out.depth = pos.w;
  gl_Position = pos;
  Out.lerp_texcoord = fract(progress_dot_framecount);
}
