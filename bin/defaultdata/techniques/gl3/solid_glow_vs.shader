#version 330
// vertex shader for drawing GLOW of solid objects

in vec3 position;
//in vec3 normal;
in vec2 uv;

    // global constants
	layout (std140) uniform GlobalBlock {
	  mat4 g_view;
      mat4 g_proj;
      vec4 g_nearfar;
	};
	
	// local constants
    uniform mat4 g_world;
    uniform mat4 g_worldnormals;
    uniform vec4 g_matdiffuse;
    uniform vec4 g_normaloffset;

out inter {
  vec4 v_world_pos;
  vec2 v_texcoord;
} Out;

void main()
{
  vec4 pos;
  pos.xyz = position;
  pos.w = 1.0;
  
  Out.v_world_pos = g_world * pos;
  vec4 viewpos = g_view * Out.v_world_pos;
  viewpos.w += 0.005;
  gl_Position = g_proj * viewpos;

  Out.v_texcoord = uv;
}
