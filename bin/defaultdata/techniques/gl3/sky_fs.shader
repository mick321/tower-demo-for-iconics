#version 330
// fragment shader for postprocessing

uniform samplerCube texCubeSky;

in inter {
  vec3 v_uv;
} In;

out vec4 fragmentColor;

void main()
{
  fragmentColor = texture(texCubeSky, In.v_uv);
}