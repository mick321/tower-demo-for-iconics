#version 330
// fragment shader for drawing GLOW of solid objects

    // global constants
	layout (std140) uniform GlobalBlock {
	  mat4 g_view;
      mat4 g_proj;
      vec4 g_nearfar;
	};
	
	// local constants
    uniform mat4 g_world;
    uniform mat4 g_worldnormals;
    uniform vec4 g_matdiffuse;
    uniform vec4 g_normaloffset;

uniform sampler2D texDiffuse;

in inter {
  vec4 v_world_pos;
  vec2 v_texcoord;
} In;

out vec4 fragmentColor;

void main()
{
  fragmentColor = g_matdiffuse * texture(texDiffuse, In.v_texcoord);
}