#version 330
// vertex shader for simple drawing

in vec2 position;
in vec4 color;
in vec2 uv;
in vec2 blend;

uniform vec4 g_correctxy;

out inter {
  vec4 v_color;
  vec2 v_uv;
  vec2 v_blend;
} Out;

void main()
{  
  gl_Position.x = position.x * g_correctxy[0] + g_correctxy[2];
  gl_Position.y = position.y * g_correctxy[1] + g_correctxy[3];
  gl_Position.z = 1.f;
  gl_Position.w = 1.f;
  Out.v_color = color;
  Out.v_uv    = uv;
  Out.v_blend = blend;
}
