#version 330
// fragment shader for simple drawing

in inter {
  vec4 v_color;
} In;

out vec4 fragmentColor;  

void main()
{  
  fragmentColor = In.v_color;
}