#version 330
// vertex shader for drawing GLOW of solid objects

in vec3 position;
in vec2 uv;
in vec4 boneIW;

#ifdef##USE_UBO

    // global constants
	layout (std140) uniform GlobalBlock {
	  mat4 g_view;
      mat4 g_proj;
      vec4 g_nearfar;
	};
	
	// local constants
    uniform mat4 g_world;
    uniform mat4 g_worldnormals;
    uniform vec4 g_matdiffuse;
    uniform vec4 g_normaloffset;
    
    // bone constants
    const int g_bonearraymaxcount = ##MAX_BONE_COUNT * 3;
    uniform vec4 g_bonearray[g_bonearraymaxcount];

#else
	uniform mat4 g_viewproj;
	uniform mat4 g_world;
    uniform mat4 g_worldnormals;
	uniform vec4 g_matdiffuse;
#endif

out inter {
  vec4 v_world_pos;
  vec2 v_texcoord;
} Out;

vec4 compute_skinning(vec4 objpos)
{
  vec4 rtn = vec4(0,0,0,1);
  for (int i = 0; i < 4; i++)
  {
    int index = int(boneIW[i]) * 3;
    float weight = fract(boneIW[i]) * 2;
    
    vec3 temp;
    temp.x = dot(g_bonearray[index], objpos);
    temp.y = dot(g_bonearray[index+1], objpos);
    temp.z = dot(g_bonearray[index+2], objpos);
    rtn.xyz += temp * weight;
  }
  return rtn;
}

void main()
{
  vec4 pos;
  pos.xyz = position;
  pos.w = 1.0;
  
  Out.v_world_pos = g_world * compute_skinning(pos);
  vec4 viewpos = g_view * Out.v_world_pos;
  viewpos.w += 0.01;
  gl_Position = g_proj * viewpos;
  
  Out.v_texcoord = uv;
}
