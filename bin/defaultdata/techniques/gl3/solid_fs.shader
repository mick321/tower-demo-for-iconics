#version 330
// fragment shader for drawing solid objects

#include "phong.shaderpart"

    // global constants
	layout (std140) uniform GlobalBlock {
		mat4 g_viewproj;
		vec4 g_cameraposition;
        
        mat4 g_shadowmatrix;
        vec4 g_shadowoptions;
	};
	
	// local constants
	const int g_lightmaxcount = ##MAX_LIGHT_COUNT;
    uniform mat4 g_world;
    uniform mat4 g_worldnormals;
    uniform vec4 g_lightambient;
    uniform vec4 g_lightoptions;
    uniform vec4 g_lightvecarray[g_lightmaxcount];
    uniform vec4 g_lightcolorarray[g_lightmaxcount];
    uniform vec4 g_lightoptionsarray[g_lightmaxcount];
    uniform vec4 g_matambient;
    uniform vec4 g_matdiffuse;
    uniform vec4 g_matspecular;
#ifdef##USE_TEXARR
    uniform vec4 g_mattextures;
#endif
#ifdef##TERRAIN
    uniform vec4 g_terrainsize;
#endif
#ifdef##NORMALMAP
    uniform vec4 g_normalmapintensity;
#endif
    uniform vec4 g_shadowsobject;

uniform sampler2D texDiffuse;
#ifdef##SKY_LIGHTING
uniform samplerCube texSkyDiffuse;
#endif
#ifdef##SKY_LIGHTING_REFLECT
uniform samplerCube texSkySpecular;
#endif
#ifdef##USE_TEXARR
uniform sampler2D texSpecularMap;
#ifdef##NORMALMAP
uniform sampler2D texNormalMap;
#endif
#endif

#ifdef##TERRAIN
uniform sampler2D texTerrain;
#endif

uniform sampler2D texShadowMap;

in inter {
  vec4 v_world_pos;
#ifdef##NORMALMAP
  mat3 mat_tbn;
#else
  vec3 v_world_normal;
#endif
  vec2 v_texcoord;
#ifdef##TERRAIN
  vec2 v_texcoordTer;
#endif
} In;

out vec4 fragmentColor;

vec2 poissonDisk[5] = vec2[5](
    vec2(-0.6459163, -0.4206268),
    vec2(-0.1028868, 0.4963762),
    vec2(0.1938148, -0.8227446),
    vec2(0.8453812, -0.3342402),
    vec2(0.6486317, 0.6963109)
);

void main()
{
#ifdef##MATERIAL
  vec4 tex = texture(texDiffuse, In.v_texcoord);
  vec4 ambientMaterial = g_matambient * tex;
  vec4 diffuseMaterial = g_matdiffuse * tex;
  vec4 specularMaterial = g_matspecular;
  #ifdef##USE_TEXARR
  if (g_mattextures.x != 0)
    specularMaterial *= texture(texSpecularMap, In.v_texcoord).r;
  #endif
  #ifdef##TERRAIN
    vec4 terrainGlobalTex = texture(texTerrain, In.v_texcoordTer);
    ambientMaterial *= terrainGlobalTex;
    diffuseMaterial *= terrainGlobalTex;
  #endif
  
	#ifdef##LIGHTING
      #ifdef##NORMALMAP
      vec3 n_normal = In.mat_tbn * mix(vec3(0,0,1), 2*texture(texNormalMap, In.v_texcoord).xyz-vec3(1), g_normalmapintensity.x);
      n_normal = normalize(n_normal);
      #else
      vec3 n_normal = normalize(In.v_world_normal);
      #endif
      
	  #ifdef##SKY_LIGHTING
	    fragmentColor = ambientMaterial * (g_lightambient + texture(texSkyDiffuse, n_normal));
	  #else
	    fragmentColor = ambientMaterial * g_lightambient;
	  #endif
	  vec3 n_tocamera = normalize(g_cameraposition.xyz - In.v_world_pos.xyz); // (move (part) to VS?)
	  
	  int lightCount = int(g_lightoptions.w); // (move to VS?)
	  vec3 lightDirection;
      
      #ifdef##SKY_LIGHTING_REFLECT
      fragmentColor += specularMaterial * tex * texture(texSkySpecular, reflect(-n_tocamera, n_normal));
      #endif
      
      float lightAttShadow = 1.0;
      if (g_shadowoptions.z * g_shadowsobject.x > 0)
      {
        vec4 shadowCoord = g_shadowmatrix * (In.v_world_pos + vec4(n_normal * 0.2, 0));
        float shadowD = 1;
        float shadowDThresh = dot(shadowCoord.xy / shadowCoord.w, shadowCoord.xy / shadowCoord.w);
        shadowDThresh = smoothstep(0.5, 1.0, shadowDThresh);
        if (shadowDThresh < 1)
        {
            vec2 samplept = shadowCoord.xy * 0.5 + vec2(0.5);
            float d = (0.5 * shadowCoord.z / shadowCoord.w + 0.5);
            shadowD = 0;
            shadowD += step(d, texture(texShadowMap, samplept + poissonDisk[0] * g_shadowoptions.xy).x);
            shadowD += step(d, texture(texShadowMap, samplept + poissonDisk[1] * g_shadowoptions.xy).x);
            shadowD += step(d, texture(texShadowMap, samplept + poissonDisk[2] * g_shadowoptions.xy).x);
            shadowD += step(d, texture(texShadowMap, samplept + poissonDisk[3] * g_shadowoptions.xy).x);
            shadowD += step(d, texture(texShadowMap, samplept + poissonDisk[4] * g_shadowoptions.xy).x);
            shadowD *= 0.2;
        }
        lightAttShadow = max(shadowD, shadowDThresh);
      }
      
	  for (int i = 0; i < lightCount; i++)
	  {
		float w = g_lightvecarray[i].w;
		lightDirection = w * In.v_world_pos.xyz + (1 - 2 * w) * g_lightvecarray[i].xyz;
		float lightDirectionLength = length(lightDirection);
		vec3 n_lightdirection = lightDirection / lightDirectionLength;
		float attenuation = compExpAtt(g_lightoptionsarray[i].x, lightDirectionLength, g_lightoptionsarray[i].y);
		vec4 lightcolor = vec4(g_lightcolorarray[i].xyz * g_lightcolorarray[i].w, 1.0);
		fragmentColor += max(lightAttShadow, 1-g_lightoptionsarray[i].w) * lightcolor * phong_noambient(n_normal, n_lightdirection, n_tocamera, diffuseMaterial, specularMaterial) * mix(1.0, attenuation, w);
	  }
	#else
	  fragmentColor = max(ambientMaterial, diffuseMaterial);
	#endif
	
#else
  vec4 diffuseColor = texture(texDiffuse, In.v_texcoord);
  #ifdef##TERRAIN
    diffuseColor *= texture(texTerrain, In.v_texcoordTer);
  #endif
  fragmentColor = dot(normalize(In.v_normal), vec3(0, 0, -1)) * (diffuseColor); // temporary!
#endif
}