#version 330
// vertex shader for postprocessing

in vec2 position;

uniform mat4 g_invview;
uniform mat4 g_invproj;

out inter {
  vec3 v_uv;
} Out;

void main()
{
  vec4 temp = g_invproj * vec4(position.x, position.y, -1.0, 1);
  temp = vec4(temp.xy, -1, 0);
  Out.v_uv = (g_invview * temp).xyz;
  gl_Position = vec4(position.xy, 1, 1);
}
